//-------------------------------------------------------------------------------------
// Arduino code that reads from six separate pressure sensors.
// Four load cells, one analog pressure censor, one digital pressure sensor.
//-------------------------------------------------------------------------------------

/**
 * @note PIN LAYOUT
 * fsr 1 - 8 -----> A0 - A7
 * Air pressure --> A8
 * Motor voltage -> A15
 * Motor Current -> A10
 * Motor Temp ----> A9
 * TENS ----------> A13
 * 
 * Encoder+ ------> D2
 * Encoder- ------> D3
 * Flow sensor ---> D4
 */

#include <Arduino.h>
#include <HardwareSerial.h>
#include <EEPROM.h>
#include <Wire.h>
#include <Encoder.h>

const unsigned int analog_start = 54;
const unsigned int num_readings = 1;

/** 
 * @note Divisor was kept when it shouldnt' have been, but it added stability
 * Keeping it would add stability but lose resolution
 */
const unsigned int divisor = 1; // 2^1 = 2
unsigned int num_sensors = 8;
unsigned int weight_offset[8];
int32_t encoder_offset[4];

/** Read once every 10 seconds */
#define WAIT_TIME 1

// #define DEBUG
#ifdef DEBUG
#define dbprint(__VA_ARGS__) Serial.print(__VA_ARGS__)
#define dbprintln(__VA_ARGS__) Serial.println(__VA_ARGS__)
#else
#define dbprint(__VA_ARGS__) ;
#define dbprintln(__VA_ARGS__) ;
#endif // DEBUG

unsigned long time_offset;
unsigned long prev_t;

Encoder motor(2, 3);    /// pin 2 forward, pin 4 reverse

/**
 * @struct
 * weight1-8 are weight sensor values
 * Weight should be an integer value
 * pressure1-2 are pressure sensor values
 * pressure should be an integer value
 */
typedef struct {
  unsigned long time;
  uint16_t      fsr[8];
  uint16_t      pressure;
  uint16_t      flow;
  uint16_t      current;
  uint16_t      voltage;
  uint16_t      temperature;
  int32_t       encoder;
  uint16_t      TENS;
} values_t;
const unsigned int values_s = sizeof(values_t);

/**
 * Writes all the values to the serial port 
 * @param v values_t that is to be sent
 */
void serialWriteValues(values_t v) {
  byte * b = (byte *) &v;
  Serial.write(b, values_s);
  Serial.print("\n\n\n");
}

/**
 * Initializes weight sensors
 * @note currently uses weight sensor configuration
 */
void fsrInit() {
  num_sensors += analog_start;

  for (uint8_t i = 0; i < 7; i++){
    weight_offset[i] = 0;
  }
}

/**
 * Function to read weight sensors. Change as needed
 */
void fsrRead(values_t *values) {
  unsigned int total[8] = {0,0,0,0,0,0,0};
  unsigned int index;
  for (unsigned int in_pin = analog_start; in_pin < num_sensors; in_pin++) {
    index = in_pin - analog_start;
    // go through readings
    for (unsigned int readIndex = 0; readIndex < num_readings; readIndex++) {
      total[index] += analogRead(in_pin) - weight_offset[index];
      if (total[index] > 1024) {
        total[index] = 0;
      }
      /*dbprint(index); dbprint(" ");
      dbprintln(total[index]);*/
    }
    // Return average
    values->fsr[index] = total[index] >> divisor;
    // Prevents the value from jumping around
    if (values->fsr[index] < 5) {
      values->fsr[index] = 0;
    }
  }
}

/**
 * Function that reads in encoder data
 * @param[out] values values_t Variables
 */
void encoderRead(values_t *values) {
  values->encoder = motor.read() - encoder_offset[0];
}

/**
 * Function that reads in TENS values
 * @param[out values values_t Variables
 */
void tensRead(values_t *values) {
  values->TENS = analogRead(A13);
}

/**
 * Reads in in flow rate sensor
 * @param[out] values values_t variable
 */
void flowRead(values_t *values, uint32_t tempTime) {
  static byte state = LOW;
  static uint32_t prev_t = 0;
  static uint32_t flow_data = 0;

  // read every second
  if ((tempTime - prev_t) > 1000) {
    values->flow = flow_data;
    flow_data = 0;
    prev_t = tempTime;
  } else {
    values->flow = 0;
    if (digitalRead(4) != state) {
      state = digitalRead(4);
      flow_data++;
    }
  }
}

/**
 * Function that calibrates encoder offset values
 */
void encoderCalibrate() {
  encoder_offset[0] = motor.read();
}

/**
 * function that calibrates the fsr sensors 
 */
void fsrCalibrate() {
  // zero out
  for (uint8_t j = 0; j < 8; j++) {
    weight_offset[j] = 0;
  }
  // number of samples
  for (uint8_t i = 0; i < 8; i++) {
    // go through samples
    for (uint8_t sensor = 0; sensor < 8; sensor++) {
      weight_offset[sensor] += analogRead(sensor+analog_start);
    }
  }

  // Average each
  for(uint8_t k = 0; k < 8; k++) {
    weight_offset[k] = weight_offset[k] >> 3;
    dbprint("offset "); dbprint(k); dbprint(": ");
    dbprintln(weight_offset[k]);
  }
}

/**
 * Scans serial port 0 for any activity
 */
void serialScan(void) {
  if (Serial.available() > 0) {
    switch(Serial.read()) {
      case '`':  // 96 value
        EEPROM.write(0, 96);
        dbprintln("Set 9600 baud");
        break;

      case 's': // 115 values
        EEPROM.write(0, 115);
        dbprintln("Set 115200 baud");
        break;

      case 'c':
        // calibrate function
        dbprintln("Calibrating");
        fsrCalibrate();
        encoderCalibrate();
        break;

      case '4':
        EEPROM.write(1, 4);
        dbprintln("num_sensors = 4");
        break;

      case '8':
        EEPROM.write(1, 8);
        dbprintln("num_sensors = 8");
        break;

      default:
        // do nothing
        break;
    }
  }
}

/************************************************
 * setup
 * **********************************************/
void setup() {
  Serial.begin(115200);
  dbprintln("Starting...");
  fsrInit();
  encoder_offset[0] = 0;

 time_offset = 0;
 dbprint("Leaving Setup");
} // void setup()


/************************************************
 * loop()
 * **********************************************/
void loop() {
  static values_t values;

  // Init time_offset
  if (time_offset == 0) {
    time_offset = millis();
    prev_t = time_offset;
    values.flow = 0;
  }

  //get smoothed value from data set + current calibration factor
  if (((millis() - time_offset) - prev_t) > WAIT_TIME) {
    prev_t = millis() - time_offset;

    values.time = prev_t;
    fsrRead(&values); // This can be changed in the function above
    values.pressure = analogRead(A8);
    values.voltage = analogRead(A15);
    values.current = analogRead(A10);
    values.temperature = analogRead(A9);
    //values.voltage[1] = analogRead(A10);
    //values.voltage[2] = analogRead(A11);
    //values.voltage[3] = analogRead(A12);
    encoderRead(&values);
    tensRead(&values);
    flowRead(&values, prev_t);

    serialWriteValues(values);
  }

  serialScan();
}
