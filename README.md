# EMMS sensor system
[TOC]

## Overview
This is the sensor system for the EMMS system. The sensor system is divided into 2 parts: an Arduino-based sensor apparatus and Node-based sensor interface.

The two folders of note is `Arduino`, which contains the arduino code, and `emms_application`, which contains the interface code.

The `test_set` folder contains test arduino code to work with application. This essentially produces fake, but usable data when testing or debugging.


## Protocol concept
The protocol used is a taking each point of data and putting them all together in a long byte string. Each data point is either 2 or 4 bytes long.

`[timestamp][fsr_1][fsr_2][fsr_3][fsr_4][fsr_5][fsr_6][fsr_7][fsr_8][air_pressure][current_1][current_2][current_3][current_4][voltage_1][voltage_2][voltage_3][voltage_4][temperature][encoder][\n\n\n]`

The values sent are all discrete byte values, and are scaled and interpreted on the interface side.

## Hardware setup
1. Place the sensor board to the appropriate pins of the Arduino board
2. Hookup all the sensors to their designated location
3. Give the sensor board its dedicated power
4. Ensure that Aref is plugged into the Arduino board
5. With a USB-A to USB-B cable, connect the Arduino to the computer

If you wish to setup the box as well, you can also connect a USB-A to USB-B cable from the box to the computer.

## Interface setup
NOTE: This assumes you're using the full application

1. Open the `EMMS Test Utility` application
2. Find the Sensor board (and the box if you are using it) and enable the ports
    - On Windows it'll be something other than `COM1`
    - On Linux it is typically `/dev/ttyACM*`
3. Click the `Begin Logging` button to begin logging the data

## Interface (dev) setup
If you are using the dev setup, please follow the instructions in the `emms_application/README.md` to have the development setup done.

1. Open a console or terminal
    - On windows 10 it is `Powershell`, on windows 7, `Command Prompt`
    - On Linux, use the default terminal (`ctl+alt+t`)
2. Navigate to the `emms_application` directory
    - That would be `cd path/to/emms20team/emms_application`
3. Type `npm start` and then `Enter` in to the terminal to start the application
    - Troubleshooting help can be found in the `emms_application` directory

## GIT usage
This project uses git for its version control. 

### Branch structure
Here are how the branches are to be used.

- Master
    - The main production branch
    - Everything in this branch is assumed to be stable and working
    - No development should be happening here, and most of its features should be merged from the release branch
- Devel
    - Development branch
    - This is the main source of development. Features should be merged here, and updates should be worked on in this branch
- Release/
    - Pre-release branch
    - This branch is where minor bug fixes and minor UX Improvments should go before being merged into the the master branch.
    - This branch should be deleted once it has been successfully merged with the master and devel branches
- Feature/
    - Feature branches
    - These should contain individual features, such as logging upgrades, new sensor integration, any major UI changes.
- Hotfix/
    - Any bugs found in the master branch that cannot wait for a release should be fixed in Bugfix branches and merged into both Master and Devel branches.
- Bugfix/
    - Any bugs that are being worked on in devel branch should usually be their own branch
    - Once completed, they shoudl be merged into the Devel branch, but they do not have to be deleted

### Common commands
- Add and commit progress

`git add <files>`

`git commit`

This will bring up vim. Press `i` to insert, and put in your commit. 
When done, press `<esc>` to leave insert mode. Type `:wq` to save and exit vim. 

- Check what's changed

`git diff <files>`
This will open less and show you the difference between the unstaged files (files not added) and the last commit
    
- Create a new branch

`git checkout -b <branchname>`
This will create and move you to the new branch

- Push brand new branch to repo

`git push -u <branchname>`

- Pull latest changes from repo

`git pull`

This will pull the latest changes of the current branch you are on

- See which branch you are on

`git branch`
Will display the current branch as green with an asterisk

- Merge your branches changes with another<br>

`git merge <branchname>`

This will merge the current branch with the specified branch. If there are conflicts (points where the changes are in the same location but different) you may have to resolve those conflicts.
You can do so in Visual Studio code very easily, or do so in another IDE Or text editor of choice. Make sure the code is able to run and test the merge to see if anything broke.

### Commits
When committing, your commit messages should be structured like an email, with a subject (the main point of this commit) and the body (more information on why the changes were made).

- Make sure that the subject line is in the imperitive voice.
    -- It should finish the sentence "If applied, this commit will ____."
- Don't put a period in the subject line
- Keep it within 50 characters (vim will turn red when you go over)
- Have an empty line between the subject and body (vim will turn red when you violate the rule)
- The body can be free form text or bullet points, whichever makes more sense
- The subject line is the most important, as it is will be the thing that will most likely be read when merging commits and used for release notes
- Your commits should be pretty small, and only have a few changes at a time
- If you cannot some up the changes in one 50 character sentence, you should make smaller commits
- You can use `git add -i` to add parts of files to create smaller commits, if necessary