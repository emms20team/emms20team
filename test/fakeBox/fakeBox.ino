#include <Arduino.h>

#define MOTOR_WAIT_TIME 3000 // Wait time before it changes motors

byte motorNum;

struct values {
    unsigned long time;
    unsigned long HWTime;
    byte delta;
    byte pwm;
    unsigned long motorOnTime[4];
    unsigned long motorOffTime[4];
};

unsigned long motorOnTime[4];
unsigned long motorOffTime[4];

void setup() {
    Serial.begin(115200);
    Serial.println("begin");
    motorNum = 0;
    for(int i =0; i < 4; i++ ) {
        motorOffTime[i] = millis();
        motorOnTime[i] = millis();
    }
}

void loop() {
    Serial.print(millis()); Serial.print(','); 
    Serial.print(millis()+2); Serial.print(',');
    Serial.print("3,100,");

    switch(motorNum) {
        case 0:
            Serial.print(millis()); Serial.print(",");
            Serial.print(motorOffTime[0]); Serial.print(",0,");
            Serial.print(motorOnTime[1]); Serial.print(",");
            Serial.print(millis()); Serial.print(",0,");
            Serial.print(motorOnTime[2]); Serial.print(",");
            Serial.print(millis()); Serial.print(",0,");
            Serial.print(motorOnTime[3]); Serial.print(",");
            Serial.print(millis()); Serial.println(",0");
            break;
        case 1:
            Serial.print(motorOnTime[0]); Serial.print(',');
            Serial.print(millis()); Serial.print(",0,");
            Serial.print(millis()); Serial.print(',');
            Serial.print(motorOffTime[1]); Serial.print(",0,");
            Serial.print(motorOnTime[2]); Serial.print(',');
            Serial.print(millis()); Serial.print(",0,");
            Serial.print(motorOnTime[3]); Serial.print(',');
            Serial.print(millis()); Serial.println(",0");
            break;
        case 2:
            Serial.print(motorOnTime[0]); Serial.print(',');
            Serial.print(millis()); Serial.print(",0,");
            Serial.print(motorOnTime[1]); Serial.print(',');
            Serial.print(millis()); Serial.print(",0,");
            Serial.print(millis()); Serial.print(',');
            Serial.print(motorOffTime[2]); Serial.print(",0,");
            Serial.print(motorOnTime[3]); Serial.print(',');
            Serial.print(millis()); Serial.println(",0");
            break;
        case 3:
            Serial.print(motorOnTime[0]); Serial.print(',');
            Serial.print(millis()); Serial.print(",0,");
            Serial.print(motorOnTime[1]); Serial.print(',');
            Serial.print(millis()); Serial.print(",0,");
            Serial.print(motorOnTime[2]); Serial.print(',');
            Serial.print(millis()); Serial.print(",0,");
            Serial.print(millis()); Serial.print(',');
            Serial.print(motorOffTime[3]); Serial.println(",0");
            break;
        default:
            break;
    }

    if (millis() > (motorOnTime[motorNum] + MOTOR_WAIT_TIME)) {
        motorOnTime[motorNum] = millis();
        motorNum++;
        if (motorNum > 3) {
            motorNum = 0;
        }
        motorOffTime[motorNum] = millis();
        motorOnTime[motorNum] = motorOffTime[motorNum];
    }
}
