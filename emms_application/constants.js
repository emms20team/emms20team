const PORTS = {
  PORT_1: 1,
  PORT_2: 2,
  PORT_3: 3,
  PORT_4: 4,
};

const DATA_IDX = {
  time: 0,
  psi: 9,
  flow: 10,
  curr: 11,
  volt: 12,
  temp: 13,
  barrel: 14,
  strap: 15,
  estim: 16,
};

const BOX_DATA_IDX = {
  // [timestamp][timeDelta][pwm%][numMotors][onOffmotor1][onOffMotor2][...][motorCurrent1][motorCurrent2][...]
}

module.exports = {
  PORTS,
  DATA_IDX,
  BOX_DATA_IDX,
};
