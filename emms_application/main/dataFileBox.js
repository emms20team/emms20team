const {DataFile} = require("./dataFile");
const {BOX_DATA_IDX} = require("../constants");

class DataFileBox() extends DataFile {
  constructor(args) {
    super(args);
    this._IDX = BOX_DATA_IDX
  }
  getAnalysis() {
    let lineStartByte = 0;
    let timestamp = 0;
    this._bytes = 0;
    let currentLine = 0;

    // ESTIM - linear graph
    let {
      evalData: evalEstimData,
      getDataAnalysis: getEstimAnalysis
    } = this.processData(this._IDX.estim, this._baselineData, {
      unit: "V",
      epsilon: 30,
    });

    let data = [];

    while (this._bytes < this._stats.size && timestamp <= this._end) {
      data = []
      let buffer = new Buffer.alloc(1);
      let currentVal = "";
      let bs = "";

      while(true) {
        this._bytes += fs.readSync(this._fd, buffer, 0, buffer.length,
                                   this._bytes);
        bs = buffer.toString();

        if (bs === ",") {
          data.push(parseFloat(currentVal));
          currentVal = "";
        }
        else if (bs === "\n") {
          currentLine++;
          data.push(parseFloat(currentVal));
          lineStartByte = this._bytes
          break;
        }
        else {
          currentVal += bs;
        }
      }

      // (currentLine > num) because we dont consider the first couple entries
      // in log files. Currently we are writing some initial garbage data to
      // logs and the timestamps are often very large and will terminate the
      // file loop early.  can take out the currentline check when we fix
      // logging
      if (currentLine > 50) {
        timestamp = data[this._IDX.time];
        evalEstimData(data);
      }
    }

    // check if duration time given was too long. if so set the end time to
    // the last recorded timestamp
    if (this._bytes === this._stats.size) {
      this._end = data[this._IDX.time];
    }

    return {
      time: this._end - this._start,
      estim: getEstimAnalysis(),
    };
  }

  processMotorStateData(motorOnIdx, motorOffIdx) {
    let timestamp = 0;
    let inCycle = false;
    let cycleNum = 0;
    let cycleStartTime = 0;
    let cycleTimeTotal = 0;

    let cycleData = []

    let currCycleData = {
      startTime: 0,
      endTime: 0,
    };

    let data = [];

    let isMotorRunning = dataVal => {
      return dataVal >= 0;
    };

    let evalData = d => {
      data = d;
      timestamp = data[this._IDX.time];
      let dataVal = data[motorOnIdx] - data[motorOffIdx];

      if (inCycle) {
        if (!isMotorRunning(dataVal)) { // motor stops running
          inCycle = false;
          cycleNum++;
          currCycleData["endTime"] = timestamp;
        }
       // motor still running
        else {
          cycleTimeTotal += timestamp;
        }
      }
      // motor starts to run
      else if (isMotorRunning(dataVal)) {
        inCycle = true;
        cycleTimeTotal += timestamp;
        currCycleData["startTime"] = timestamp;
      }
    }

    let getDataAnalysis = () => {
      return {
        type: "motor",
        cyclesPerMinute: this.getCyclesPerMinute(cycleNum),
        cycleNum: cycleNum,
        cycleAvgTime: cycleNum === 0 ? 0 : cycleTimeTotal / cycleNum,
        cycleData: cycleData,
      };
    }

    return {
      getDataAnalysis: getDataAnalysis,
      evalData: evalData
    };
  }
  processMotorStateData(motorOnIdx, motorOffIdx) {
    let timestamp = 0;
    let inCycle = false;
    let cycleNum = 0;
    let cycleStartTime = 0;
    let cycleTimeTotal = 0;

    let cycleData = []

    let currCycleData = {
      startTime: 0,
      endTime: 0,
    };

    let data = [];

    let isMotorRunning = dataVal => {
      return dataVal >= 0;
    };

    let evalData = d => {
      data = d;
      timestamp = data[this._IDX.time];
      let dataVal = data[motorOnIdx] - data[motorOffIdx];

      if (inCycle) {
        if (!isMotorRunning(dataVal)) { // motor stops running
          inCycle = false;
          cycleNum++;
          currCycleData["endTime"] = timestamp;
        }
       // motor still running
        else {
          cycleTimeTotal += timestamp;
        }
      }
      // motor starts to run
      else if (isMotorRunning(dataVal)) {
        inCycle = true;
        cycleTimeTotal += timestamp;
        currCycleData["startTime"] = timestamp;
      }
    }

    let getDataAnalysis = () => {
      return {
        type: "motor",
        cyclesPerMinute: this.getCyclesPerMinute(cycleNum),
        cycleNum: cycleNum,
        cycleAvgTime: cycleNum === 0 ? 0 : cycleTimeTotal / cycleNum,
        cycleData: cycleData,
      };
    }

    return {
      getDataAnalysis: getDataAnalysis,
      evalData: evalData
    };
  }
}

module.exports = {
  DataFileBox
};
