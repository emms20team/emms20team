const {app, BrowserWindow, ipcMain} = require('electron');
const Serial = require('./serialport');
const SerialBox = require('./serialportbox');
const fs = require('fs');
const path = require('path');
const FileParser = require("./fileparser");
const DataAnalysis = require("./data_analysis");

const {PORTS} = require("../constants");

var events = require("events");

var serialPort = new Serial("COM5", 115200, "log", 1);
var serialPort2 = new Serial("COM6", 115200, "log2", 2);
var serialPort3 = new Serial("COM7", 115200, "log3", 3);
var serialPort4 = new Serial("COM8", 115200, "log4", 4);

var serialPortBox = new SerialBox("COM1", 115200, "log", "box");

var dir = path.resolve(app.getPath('userData'), "EMMS_app");
if(!fs.existsSync(dir)) {
  fs.mkdirSync(dir, { recursive: true });
}

// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let log_windows = [];
let mainWindow
function createWindow () {
  // Create the browser window.
  mainWindow = new BrowserWindow({width: 1600, height: 1000})

  // and load the index.html of the app.
  mainWindow.loadFile('./renderer/index.html')

  // Open the DevTools.
  // mainWindow.webContents.openDevTools()

  // Emitted when the window is closed.
  mainWindow.on('closed', function () {
    // Dereference the window object, usually you would store windows
    // in an array if your app supports multi windows, this is the time
    // when you should delete the corresponding element.
    mainWindow = null
    // Add this line for each serial port
    serialPort.endTimeStamp();
    serialPort2.endTimeStamp();
    serialPort3.endTimeStamp();
    serialPort4.endTimeStamp();

    while(log_windows.length > 0) {
      console.log('closing a log window.')
      log_windows[0].close();
    }
  })
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', createWindow);

// Quit when all windows are closed.
app.on('window-all-closed', function () {
  // On macOS it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== 'darwin') {
    app.quit();
  }
});

app.on('activate', function () {
  // On macOS it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (mainWindow === null) {
    createWindow();
  }
})

ipcMain.on("refresh_serial_ports", (event, arg) => {
  Serial.portList().then(
    ports => {
      serialPort.disable();
      serialPort2.disable();
      serialPort3.disable();
      serialPort4.disable();
      serialPortBox.disable();
      mainWindow.webContents.send('serial_ports', ports)
      mainWindow.webContents.send('serial_ports_2', ports)
      mainWindow.webContents.send('serial_ports_3', ports)
      mainWindow.webContents.send('serial_ports_4', ports)
      mainWindow.webContents.send('serial_box_ports', ports)
    },
    err => console.error(err)
  );
});

// Message to request enabling serial
// TODO: is this ever called?
ipcMain.on('serial_scale', (event, arg) => {
  console.log('setting scaling');
  console.log(arg);
  serialPort.scaling = arg;
});

ipcMain.on('process_data', (event, arg) => {
  event.returnValue = DataAnalysis.process(arg);
});

// keeps track of which port is selected
let selectedPort = PORTS.PORT_1;

// when user presses a zone this will change the selected port
// when a port is selected it will send data to be rendered by master chart
// if not it will not send
ipcMain.on("change_port", (event, arg) => {
  selectedPort = arg;
})

ipcMain.on('enable_serial_1', (event, arg) => {
  Serial.portList().then(list => console.log(list));

  var emitter = serialPort.enable(arg);
  emitter.on('data_1', (data) => {
    if (mainWindow) {
      mainWindow.webContents.send(
        "zone_data_1",
        [data, DataAnalysis.process(data)]
      );

      // if the selected port is 1 send data
      if (selectedPort === PORTS.PORT_1) {
        mainWindow.webContents.send(
          'serial_data',
          [data, DataAnalysis.process(data)]
        );
      }
    } else {
      console.log("disabled port 1");
      serialPort.disable();
    }
  });
  emitter.on('serial_open_1', data => {
    console.log("serial open event");
    if(mainWindow)
      mainWindow.webContents.send('serial_open_1', data);
  })
  emitter.on('serial_closed_1', data => {
    console.log("serial closed event");
    if(mainWindow)
      mainWindow.webContents.send('serial_closed_1', data);
  })
  emitter.on('serial_error_1', data => {
    console.log("serial error event 1", data);
    if(mainWindow)
      mainWindow.webContents.send('serial_error_1', data);
  })
  emitter.on('log_closed_1', data => {
    console.log("log closed event");
    if (mainWindow)
      mainWindow.webContents.send('log_closed', data);
  })
});
ipcMain.on('get_serial_ports', (event, arg) => {
  console.log("Getting serial ports for 1...");
  Serial.portList().then(
    ports => {
      mainWindow.webContents.send('serial_ports', ports)
    },
    err => console.error(err)
  );
})

ipcMain.on('enable_serial_2', (event, arg) => {
  var emitter = serialPort2.enable(arg);
  emitter.on('data_2', (data) => {
    if (mainWindow) {

      mainWindow.webContents.send(
        "zone_data_2",
        [data, DataAnalysis.process(data)]
      );

      // if selected port is 2 send data
      if (selectedPort === PORTS.PORT_2) {
        mainWindow.webContents.send(
          'serial_data',
          [data, DataAnalysis.process(data)]
        );
      }
    } else {
      serialPort2.disable();
    }
  });
  emitter.on('serial_open_2', data => {
    if(mainWindow)
      mainWindow.webContents.send('serial_open_2', data);
  })
  emitter.on('serial_closed_2', data => {
    if(mainWindow)
      mainWindow.webContents.send('serial_closed_2', data);
  })
  emitter.on('serial_error_2', data => {
    console.log("serial error event 2", data);
    if(mainWindow)
      mainWindow.webContents.send('serial_error_2', data);
  })
  emitter.on('log_closed_2', data => {
    if (mainWindow)
      mainWindow.webContents.send('log_closed', data);
  })
});
ipcMain.on('get_serial_ports_2', (event, arg) => {
  console.log("Getting serial ports for 2...");
  Serial.portList().then(
    ports => {
      mainWindow.webContents.send('serial_ports_2', ports)
    },
    err => console.error(err)
  );
})

ipcMain.on('enable_serial_3', (event, arg) => {
  var emitter = serialPort3.enable(arg);
  emitter.on('data_3', (data) => {
    if (mainWindow) {

      mainWindow.webContents.send(
        "zone_data_3",
        [data, DataAnalysis.process(data)]
      );

      // if the selected port is 3 send data
      if (selectedPort === PORTS.PORT_3) {
        mainWindow.webContents.send(
          'serial_data',
          [data, DataAnalysis.process(data)]
        );
      }
    } else {
      console.log("disabled port 3");
      serialPort3.disable();
    }
  });
  emitter.on('serial_open_3', data => {
    if(mainWindow)
      mainWindow.webContents.send('serial_open_3', data);
  })
  emitter.on('serial_closed_3', data => {
    if(mainWindow)
      mainWindow.webContents.send('serial_closed_3', data);
  })
  emitter.on('serial_error_3', data => {
    console.log("serial error event 3", data);
    if(mainWindow)
      mainWindow.webContents.send('serial_error_3', data);
  })
  emitter.on('log_closed_3', data => {
    if (mainWindow)
      mainWindow.webContents.send('log_closed', data);
  })
});
ipcMain.on('get_serial_ports_3', (event, arg) => {
  console.log("Getting serial ports for 3...");
  Serial.portList().then(
    ports => {
      mainWindow.webContents.send('serial_ports_3', ports)
    },
    err => console.error(err)
  );
})

ipcMain.on('enable_serial_4', (event, arg) => {
  var emitter = serialPort4.enable(arg);
  emitter.on('data_4', (data) => {
    if (mainWindow) {

      (!isTsIncrease(data, zone_3_prev_ts)) || dataInputError();
      zone_4_prev_ts = data[0];

      mainWindow.webContents.send(
        "zone_data_4",
        [data, DataAnalysis.process(data)]
      );

      // if selected port is 2 send data
      if (selectedPort === PORTS.PORT_4) {
        mainWindow.webContents.send(
          'serial_data',
          [data, DataAnalysis.process(data)]
        );
      }
    } else {
      serialPort4.disable();
    }
  });
  emitter.on('serial_open_4', data => {
    if(mainWindow)
      mainWindow.webContents.send('serial_open_4', data);
  })
  emitter.on('serial_closed_4', data => {
    if(mainWindow)
      mainWindow.webContents.send('serial_closed_4', data);
  })
  emitter.on('serial_error_4', data => {
    console.log("serial error event 4", data);
    if(mainWindow)
      mainWindow.webContents.send('serial_error_4', data);
  })
  emitter.on('log_closed_4', data => {
    if (mainWindow)
      mainWindow.webContents.send('log_closed', data);
  })
});
ipcMain.on('get_serial_ports_4', (event, arg) => {
  console.log("Getting serial ports for 4...");
  Serial.portList().then(
    ports => {
      mainWindow.webContents.send('serial_ports_4', ports)
    },
    err => console.error(err)
  );
})

// emms box specific serial
ipcMain.on('enable_box_serial', (event, arg) => {
  var boxEmitter = serialPortBox.enable(arg);
  boxEmitter.on('box_data', (data) => {
    if (mainWindow) {
      mainWindow.webContents.send('serial_box_data', [data, selectedPort]);
      mainWindow.webContents.send('zone_data_box', data);
    } else {
      serialPortBox.disable();
    }
  });
  boxEmitter.on('serial_box_open', data => {
    if (mainWindow)
      mainWindow.webContents.send('serial_box_open', data);
  })
  boxEmitter.on('serial_box_closed', data => {
    if (mainWindow)
      mainWindow.webContents.send('serial_box_closed', data);
  })
  boxEmitter.on('serial_box_error', data => {
    if (mainWindow)
      mainWindow.webContents.send('serial_box_error', data);
  })
});

ipcMain.on('get_serial_box_ports', (event, arg) => {
  console.log("Getting serial ports for box...");
  SerialBox.portList().then(
    ports => {
      mainWindow.webContents.send('serial_box_ports', ports)
    },
    err => console.error(err)
  );
})

ipcMain.on('getPath', (event, arg) => {
  event.returnValue = app.getPath('userData', arg) + "/EMMS_app";
});
ipcMain.on('get_logs', (event, arg) => {
  event.returnValue = fs.readdirSync(app.getPath('userData', arg) + "/EMMS_app");
});

// parse log files and send to log.js
const send_zones = (window, mainLog) => {
  let path = app.getPath("userData") + "/EMMS_app/";

  // match all log files that go with the user clicked file
  let re = new RegExp(`${mainLog.slice(0, -5)}*`);

  let zones = fs.readdirSync(path).filter(fileName => fileName.match(re));
  let zonesRunning = zones.length;

  for (let i = 0; i < zones.length; i++) {
    let emitter = new events.EventEmitter();
    let file = zones[i];
    let zonePath = path + file;
    let fileParser = new FileParser(zonePath, emitter);

    emitter.on('data', (data) => {
      if (window) {
        window.webContents.send('zone_data', data, zonePath);
      }
    });
    emitter.on('close', (data) => {
      if (--zonesRunning === 0 && window) {
        window.webContents.send('zones_close', zonePath);
      }
    });
    fileParser.enable();
  }
}

ipcMain.on('open_log', (event, arg) => {
  let log_path = app.getPath('userData', arg) + "/EMMS_app/" + arg;
  let log_window = new BrowserWindow({width: 1600, height: 1000});
  log_window.webContents.on('dom-ready', () => {
    console.log("sending log");
    // send data from main log file to window
    send_zones(log_window, arg);
  });
  log_window.loadFile('./renderer/log.html');
  log_windows.push(log_window);
  log_window.on('closed', function () {
    console.log('removing self from array');
    log_windows.splice(log_windows.indexOf(log_window), 1);
    console.log('removing self from existence');
    log_window = null;
  })
});

// Begin Serial Port/Box Logging ------------------
ipcMain.on('begin_serial_logging', (event, arg) => {
  serialPort.setTimeStamp(arg.file1);
  serialPort2.setTimeStamp(arg.file2);
  serialPort3.setTimeStamp(arg.file3);
  serialPort4.setTimeStamp(arg.file4);
  serialPortBox.setTimeStamp(arg.fileBox);
});

// End Serial Port/Box Logging ------------------
function endSerialLogging() {
  serialPort.endTimeStamp();
  serialPort2.endTimeStamp();
  serialPort3.endTimeStamp();
  serialPort4.endTimeStamp();
  serialPortBox.endTimeStamp();
}

ipcMain.on('end_serial_logging', (event, arg) => {
  endSerialLogging();
});

// Serial Port/Box disable btn clicks ----------
ipcMain.on('disable_serial_1', (event, arg) => {
  serialPort.disable(arg);
  console.log("disable_serial_1 main");
});
ipcMain.on('disable_serial_2', (event, arg) => {
  serialPort2.disable(arg);
});
ipcMain.on('disable_serial_3', (event, arg) => {
  serialPort3.disable(arg);
});
ipcMain.on('disable_serial_4', (event, arg) => {
  serialPort4.disable(arg);
});
ipcMain.on('disable_serial_box', (event, arg) => {
  serialPortBox.disable(arg);
})

// Serial Port calibrate btn clicks ----------
ipcMain.on('calibrate_serial_1', (event, arg) => {
  serialPort.calibrate();
});
ipcMain.on('calibrate_serial_2', (event, arg) => {
  serialPort2.calibrate();
})
ipcMain.on('calibrate_serial_3', (event, arg) => {
  serialPort3.calibrate();
});
ipcMain.on('calibrate_serial_4', (event, arg) => {
  serialPort4.calibrate();
})
