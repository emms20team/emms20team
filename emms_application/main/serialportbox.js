const SerialPort = require("serialport");
const Delimiter = require("@serialport/parser-delimiter");
const fs = require("fs");
const path = require("path");
var events = require("events")

class SerialBox {
  // class will be used to read values from emms control box
  // will emit events with data and write values to log file
  constructor(portName, baud, fileName, num, dataLen=70) {
    this.timeStamp = 0;
    this.timeStampSet = false;
    this.dataLen = dataLen;
    this.portName = portName;
    this.baud = 115200;
    this.fileName = fileName;
    this.emitter = new events.EventEmitter();
    this.emitterNum = num;

    this.enable = this.enable.bind(this);
    this.disable = this.disable.bind(this);
    this.isEnabled = false;
    this.beginRead = false;

    this.emitSerialData = this.emitSerialData.bind(this);
    this.handlePortOpen = this.handlePortOpen.bind(this);
    this.handlePortError = this.handlePortError.bind(this);
    this.handlePortClose = this.handlePortClose.bind(this);
  }

  // Open serial port and set up listeners for relevant events
  enable(args) {
    Object.assign(this,args);
    this.emitter.removeAllListeners();
    this.port = new SerialPort(this.portName, {baudRate: this.baud});
    this.parser = this.port.pipe(new Delimiter({delimiter: "\r\n"}));

    // events to listen to
    this.port.on("open", this.handlePortOpen);
    this.parser.on("data", this.emitSerialData);
    this.port.on("error", this.handlePortError);
    this.port.on("close", this.handlePortClose);
    this.isEnabled = true;

    return this.emitter;
  }

  logOpen(fileName) {
    this.file = fileName;
    let dir = path.dirname(this.file);
    if(!dir)
      fs.mkdirSync(dir, { recursive: true });
    this.file = fs.createWriteStream(this.file, {flags: "w"});
    console.log("File open ".concat(this.emitterNum));
  }

  logClose() {
    this.file.close();
    console.log("File closed ".concat(this.emitterNum));
    this.emitter.emit("log_closed_".concat(this.emitterNum), this);
  }

  // Sets time stamp and begins logging data
  setTimeStamp(fileName) {
    if (this.isEnabled == false) return;
    this.timeStampSet = true;
    console.log("setTimeStamp ".concat(this.emitterNum));
    this.logOpen(fileName);
  }

  endTimeStamp() {
    if (this.timeStampSet == false) return;
    this.timeStampSet = false;
    this.timeStamp = 0;
    console.log("endTimeStamp ".concat(this.emitterNum));
    this.logClose();
  }

  // close port and file stream
  disable() {
    if (this.isEnabled) {
      this.port.close();
      this.isEnabled = false;
    }
  }

  static portList() {
    // returns object like
    //{
    //  comName: "/dev/tty.usbmodem1421",
    //  manufacturer: "Arduino (www.arduino.cc)",
    //  serialNumber: "752303138333518011C1",
    //  pnpId: undefined,
    //  locationId: "14500000",
    //  productId: "0043",
    //  vendorId: "2341"
    //}
    return SerialPort.list();
  }

  /**
   *
   * @param {*} data
   * @note Return data is in the following format
   * [timestamp][timeDelta][pwm%][#motors][onOffmotor1][onOffMotor2][...][motorCurrent1][motorCurrent2][...]
   */
  emitSerialData(data) {
    data = data.toString();
    let values = data.split(',');
    if (isNaN(data[0])) return; // leave if first value is not a number
    if (this.timeStampSet == false) {
      this.tempTimeStamp = parseInt(values[0]);
      return;
    } else {
      if (this.timeStamp == 0) {
        this.timeStamp = parseInt(values[0]);
      }
    }

    if ((parseInt(values[0]) - this.timeStamp) < 0) {
      this.timeStamp = parseInt(values[0]);
    }

    let returnValues = [];
    returnValues.push(parseInt(values[0]) - this.timeStamp);
    returnValues.push(parseInt(values[2]));
    returnValues.push((parseInt(values[3])/255) * 100);
    returnValues.push((values.length - 4) / 3);

    for (let i = 4; i < values.length; i+=3) {
      returnValues.push((parseInt(values[i]) > parseInt(values[i+1]) ? 1 : 0));
    }

    // 2019-04-17 nrs Box data is incorrect
    // the following code does a translation
    for (let i = 15; i > 4; i-=3) {
      returnValues.push((parseInt(values[i]) * 0.020365));
    }
    // uncomment this code when box has been corrected
    /*for (let i = 6; i < values.length; i+=3) {
      returnValues.push((parseInt(values[i]) * 0.020365));
    }*/
    this.emitter.emit('box_data', returnValues);
    this.file.write(returnValues.join(","));
    this.file.write("\n");
    // console.log(returnValues);
  }

  handlePortOpen() {
    console.log("port open box");
    this.emitter.emit("serial_box_open");
    this.isEnabled = true;
    this.timeStampSet = false;
  }

  handlePortClose() {
    console.log("port closed box");
    this.emitter.emit("serial_box_closed",this);
    this.timeStamp = 0;
    this.timeStampSet = false;
    this.isEnabled = false;
  }

  handlePortError(err) {
    console.log("Serial port box error: " + err);
    this.emitter.emit("serial_box_error", err);
    this.emitter.removeAllListeners();
    this.timeStamp = 0;
    this.timeStampSet = false;
    this.isEnabled = false;
  }
}

module.exports = SerialBox;
