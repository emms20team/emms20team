const fs = require("fs");
const readline = require("readline");
const math = require("mathjs");
// TODO: testing
// TODO: floating baseline
// TODO: find a better way to get to start point
//          - leading numbers that === start idex will cause reader to run
//            until it reaches a value that is greater than indx

const {DATA_IDX} = require("../constants");

class DataFile {
  constructor(fileName, start, time, epsilon=0.02) {
    this._fileName = fileName;

    this._start = start;
    this._end = start + time;

    this._stats = fs.statSync(fileName);
    this._fd = fs.openSync(fileName, "r");
    this._startPos = 0;
    this._bytes = 0;

    this._occurence = {};

    this._baselineData = [];

    this._IDX = DATA_IDX;
  }

  run() {
    // read file until start timestamp is reached
    // get baseline data from start point
    // read from start point until end point evaluating data

    let lineStartByte = 0;
    let timestamp = 0;
    let currentLine = 0;
    let data;

    while (this._bytes < this._stats.size && timestamp <= this._start) {
      let buffer = new Buffer.alloc(1);
      let currentVal = "";
      let bs = "";
      data = [];

      while(true) {
        this._bytes += fs.readSync(this._fd, buffer, 0, buffer.length,
                                   this._bytes);
        bs = buffer.toString();

        if (bs === ",") {
          data.push(parseFloat(currentVal));
          currentVal = "";
        }
        else if (bs === "\n") {
          currentLine++;
          data.push(parseFloat(currentVal));
          lineStartByte = this._bytes
          break;
        }
        else {
          currentVal += bs;
        }
      }

      // dont consider the first couple entries in log files. Currently we
      // are still writing some initial garbage data to logs and the
      // timestamps are often very large and will terminate the file loop
      // early.
      // can take out the currentline check when we fix logging
      if (currentLine > 10) {
        timestamp = data[this._IDX.time];
      }
    };

    this._baselineData = data;
    return this.getAnalysis();
  }

  getAnalysis() {
    let lineStartByte = 0;
    let timestamp = 0;
    this._bytes = 0;
    let currentLine = 0;

    // PSI - linear data
    let {
      evalData: evalPsiData,
      getDataAnalysis: getPsiAnalysis
    } = this.processData(this._IDX.psi, this._baselineData, {
      unit: "psi",
      epsilon: 0.2,
      procData: this.toPsi,
      movingAvgHistory: 1
    });

    // FLOW RATE - linear data
    let {
      evalData: evalFlowData,
      getDataAnalysis: getFlowAnalysis
    } = this.processData(this._IDX.flow, this._baselineData, {
      movingAvgHistory: 1
    });

    // CURRENT - linear data
    let {
      evalData: evalCurrentData,
      getDataAnalysis: getCurrentAnalysis
    } = this.processData(this._IDX.curr, this._baselineData, {
      unit: "mA",
      epsilon: 5,
    });

    // VOLTAGE - linear data
    let {
      evalData: evalVoltData,
      getDataAnalysis: getVoltAnalysis
    } = this.processData(this._IDX.volt, this._baselineData, {
      unit: "V",
      epsilon: 10,
    });

    // TEMPERATURE - static data
    let {
      evalData: evalTempData,
      getDataAnalysis: getTempAnalysis
    } = this.processStaticData(this._IDX.temp, "F");

    // BARREL POSITION - linear data
    let {
      evalData: evalBarrelData,
      getDataAnalysis: getBarrelAnalysis
    } = this.processData(this._IDX.barrel, this._baselineData, {
      epsilon: 20,
      movingAvgHistory: 1
    });

    // STRAP POSITION - same as barrel just different scaling
    let {
      evalData: evalStrapData,
      getDataAnalysis: getStrapAnalysis
    } = this.processData(this._IDX.strap, this._baselineData, {
      epsilon: 1,
      movingAvgHistory: 1
    });

    // ESTIM - linear graph
    let {
      evalData: evalEstimData,
      getDataAnalysis: getEstimAnalysis
    } = this.processData(this._IDX.estim, this._baselineData, {
      unit: "V",
      epsilon: 10,
      movingAvgHistory: 1
    });

    let data = [];

    while (this._bytes < this._stats.size && timestamp <= this._end) {
      data = []
      let buffer = new Buffer.alloc(1);
      let currentVal = "";
      let bs = "";

      while(true) {
        this._bytes += fs.readSync(this._fd, buffer, 0, buffer.length,
                                   this._bytes);
        bs = buffer.toString();

        if (bs === ",") {
          data.push(parseFloat(currentVal));
          currentVal = "";
        }
        else if (bs === "\n") {
          currentLine++;
          data.push(parseFloat(currentVal));
          lineStartByte = this._bytes
          break;
        }
        else {
          currentVal += bs;
        }
      }

      // (currentLine > num) because we dont consider the first couple entries
      // in log files. Currently we are writing some initial garbage data to
      // logs and the timestamps are often very large and will terminate the
      // file loop early.  can take out the currentline check when we fix
      // logging
      if (currentLine > 50) {
        timestamp = data[this._IDX.time];
        evalPsiData(data);
        evalFlowData(data);
        evalCurrentData(data);
        evalVoltData(data);
        evalTempData(data);
        evalBarrelData(data);
        evalStrapData(data);
        evalEstimData(data);
      }
    }

    // check if duration time given was too long. if so set the end time to
    // the last recorded timestamp
    if (this._bytes === this._stats.size) {
      this._end = data[this._IDX.time];
    }

    return {
      time: this._end - this._start,
      psi: getPsiAnalysis(),
      flow: getFlowAnalysis(),
      current: getCurrentAnalysis(),
      voltage: getVoltAnalysis(),
      temperature: getTempAnalysis(),
      barrel: getBarrelAnalysis(),
      strap: getStrapAnalysis(),
      estim: getEstimAnalysis(),
    };
  }

  // this function is used to process data for linear graphs
  // @arg1 dataIdx - index for data to process
  // @arg2 baselineData - line of data at start time first valid line of data
  // @arg3 config options
  //    - unit:             string repr unit of measurement
  //
  //    - epsilon:          value from baseline before data is considered to
  //                        be in a cycle
  //
  //    - movingAvgHistory: number of values to consider for moving average
  //
  //    - procData:         this function will be called on all raw values in
  //                        log file and the return value will be used instead
  //                        of raw value leave undefined if data in log file
  //                        is already in correct units
  //
  // @return object with two functions
  //    - evalData
  //        call this function giving it each line of data you would like to
  //        analyze
  //
  //        @arg1 data - line of data in log file
  //
  //    - getDataAnalysis
  //        this function will return an object analyzing data given to
  //        evalData
  //
  //        @return - analysis of data fed to eval data
  processData(dataIdx, baselineData, {
    unit = "",
    epsilon = 0.1,
    movingAvgHistory = 8,
    procData,
  }) {
    let baseline = baselineData
      ? procData ? procData(baselineData[dataIdx]) : baselineData[dataIdx]
      : 0;

    let timestamp = 0;

    let inCycle = false;
    let cycleNum = 0;
    let cycleArea = 0;
    let maxPeak = 0;
    let maxPeakSum = 0;

    let currentPeak = 0;
    let currentArea = 0;

    let inValley = false;
    let valleyNum = 0;
    let minValley = 0;
    let minValleySum = 0;

    let currentValley = 0;

    let cycleStartTime = 0;
    let cycleTimeTotal = 0;

    let currCycleData = {
      startTime: 0,
      peakTime: 0,
      endTime: 0,
      peakValue: 0,
    };

    let cycleData = [];
    let mAvgHistory = [];
    let data = [];

    let evalData = d => {
      data = d;

      timestamp = data[this._IDX.time];
      let dataVal = procData ? procData(data[dataIdx]) : data[dataIdx];
      let relativeVal = dataVal - baseline;

      // floating average
      mAvgHistory.push({val: relativeVal, time: timestamp});
      if (mAvgHistory.length > movingAvgHistory) {
        mAvgHistory.shift();
      }
      let floatingAvg = mAvgHistory.reduce((a, b) => a + b.val, 0)
        / mAvgHistory.length;

      if (timestamp >= this._start) {
        if (timestamp <= this._end) {

          if (inCycle) {
            if (floatingAvg < epsilon) {
              cycleNum++;
              cycleArea += currentArea;
              maxPeakSum += currentPeak;
              maxPeak = (currentPeak > maxPeak) ? currentPeak : maxPeak;

              cycleTimeTotal += timestamp - cycleStartTime;

              // gather cycleData
              currCycleData["endTime"] = timestamp;
              currCycleData["peakValue"] = currentPeak;
              cycleData.push(Object.assign({}, currCycleData));
              currCycleData = {};

              // reset
              cycleStartTime = 0;
              currentArea = 0;
              currentPeak = 0;
              inCycle = false;
            }
            // in cycle handle current dataVal
            else {
              currentArea += relativeVal;

              if (relativeVal > currentPeak) {
                currentPeak = relativeVal;
                currCycleData["peakTime"] = timestamp;
              }
            }
          }

          // gather valley dataVal
          else if (inValley) {
            if (floatingAvg > -epsilon) {
              valleyNum++;
              minValleySum += currentValley;
              minValley = (currentValley < minValley)
                ? currentValley : minValley;

              currentValley = 0;
              inValley = false;
            }
            else {
              if (relativeVal < currentValley) {
                currentValley = relativeVal;
              }
            }
          }

          // begin of cycle detected
          else if (floatingAvg > epsilon) {
            inCycle = true;
            for (let i = 0; i < mAvgHistory.length; i++) {
              let {val, time} = mAvgHistory[i];
              if (val > epsilon) {
                if (cycleStartTime === 0) {
                  cycleStartTime = time;
                  currCycleData["startTime"] = time;
                }
                if (val > currentPeak) {
                  currentPeak = val;
                  currCycleData["peakTime"] = timestamp;
                }
                currentArea += val;
              }
            }
          }

          // begin of valley detected
          else if (floatingAvg < -epsilon) {
            inValley = true
            for (let i = 0; i < mAvgHistory.length; i++) {
              let {val, time} = mAvgHistory[i];
              if (val < currentValley) {
                currentValley = val;
              }
            }
          }
        }
      };
    }

    let getDataAnalysis = () => {
      return {
        type: "linear",
        unit: unit,
        baseline: baseline,
        cyclesPerMinute: this.getCyclesPerMinute(cycleNum),
        cycleNum: cycleNum,
        cycleAvgTime: cycleNum === 0 ? 0 : (cycleTimeTotal / cycleNum),
        cycleAvgArea: cycleNum === 0 ? 0 : (cycleArea / cycleNum),
        peakMax: maxPeak,
        peakAvg: cycleNum === 0 ? 0 : (maxPeakSum / cycleNum),
        valleyNum: valleyNum,
        valleyMin: minValley,
        valleyAvg: valleyNum === 0 ? 0 : (minValleySum / valleyNum),
        cycleData: cycleData,
        variance: cycleData.length === 0 ? 0 : math.var(cycleData.map(e => e["peakValue"]))
      };
    }

    return {
      getDataAnalysis: getDataAnalysis,
      evalData: evalData
    };
  }

  processStaticData(dataIdx, unit, procData) {
    let sum = 0;
    let numData = 0;
    let min = Infinity;
    let max = -Infinity;

    let evalData = d => {
      let val = d[dataIdx];
      sum += val;
      numData++;
      if (val < min) {
        min = val;
      }
      if (val > max) {
        max = val;
      }
    }

    let getDataAnalysis = () => {
      return {
        type: "static",
        unit: unit,
        max: max === -Infinity ? 0 : max,
        min: min === Infinity ? 0 : min,
        avg: numData === 0 ? 0 : sum / numData,
      }
    }

    return {
      getDataAnalysis: getDataAnalysis,
      evalData: evalData
    };
  }

  // get the cycles per minute of a value
  getCyclesPerMinute(cycleNum) {
    let cpm = cycleNum / (this._end - this._start) * 60000
    return isNaN(cpm) ? 0 : cpm;
  }

  // convert raw stored value to psi
  toPsi(val) {
    return (val / 1.8) / 51.7;
  }
}

const analyzeData = ({cycleData: cd1}, {cycleData: cd2}) => {
  let startDiffSum = 0;
  let endDiffSum = 0;
  let peakDiffSum = 0;
  let peakValueDiffSum = 0;

  let len = Math.min(cd1.length, cd2.length);

  for (let i = 0; i < len; i++) {
    let cdLeft = cd1[i];
    let cdRight = cd2[i];
    startDiffSum += cdLeft.startTime - cdRight.startTime;
    endDiffSum += cdLeft.endTime - cdRight.endTime;
    peakDiffSum += cdLeft.peakTime - cdRight.peakTime;
    peakValueDiffSum += cdLeft.peakValue - cdRight.peakValue;
  }

  return {
    avgStartDiff: len === 0 ? 0 : startDiffSum / len,
    avgEndDiff: len === 0 ? 0 : endDiffSum / len,
    avgPeakDiff: len === 0 ? 0 : peakDiffSum / len,
    avgPeakValueDiff: len === 0 ? 0 : peakValueDiffSum / len,
  }
}

module.exports = {
  DataFile,
  analyzeData
};
