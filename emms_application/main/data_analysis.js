const math = require('mathjs');

class heldData {
  constructor() {
    this.encoderData = [];
    this.tempData = [];
    this.curentTemp = 0;
    this.max = 0;
    this.min = 0;
    this.findMid = 0; // count for finding midpoint (at 2 it will find midpoint)
    this.atMax = false; // used to figure out if we have gotten to a max or not
    this.goingMax = true; // assume always rising
    this.atMin = false; // used to figure out if we have gotten to a min or not
    this.goingMin = true;
    this.result = 0;
  }

  addEncoderData(newData) {
    this.encoderData.push(newData);
    // when surpassing max and going toward the max
    if (newData > this.max && this.goingMax) {
      // console.log('>this.max')
      this.max = newData;
      this.goingMax = true;
      this.goingMin = false;
    } else if (newData < this.min && this.goingMin) {
      // when going below the min and going toward min
      // console.log('<this.min');
      this.min = newData;
      this.goingMin = true;
      this.goingMax = false;
    } else if (newData < this.max && this.goingMin === false && this.findMid == 0) {
      // when starting to go below the max
      // console.log('data<this.max');
      this.atMax = this.max;
      this.findMid++;
      // console.log(this.findMid);
    } else if (newData > this.min && this.goingMax === false && this.findMid == 0) {
      // when starting to go above the max
      // console.log('data>this.min');
      this.atMin = this.min;
      this.findMid++;
      // console.log(this.findMid);
    }  else if (this.findMid > 2) {
      // find mid point
      // console.log('calculateMid');
      this.result = (this.atMax + this.atMin) / 2;
      // console.log(this.atMax);
      // console.log(this.atMin);
      // console.log(this.result);
      if (this.goingMax) {
        // console.log('was going max');
        this.max = this.atMin;
        this.min = newData;
        this.goingMax = false;
        this.goingMin = true;
      } else {
        // console.log('was going min');
        this.min = this.atMax;
        this.max = newData;
        this.goingMin = false;
        this.goingMax = true;
      }

      this.findMid = 0;
    } else if (newData == this.min || newData == this.max) {
      // do nothing
      // console.log('matching data');
    } else {
      this.findMid++;
      // console.log('Increment findMid');
      // console.log(this.findMid);
    }
  }

  addTempData(data) {
    let delta = 10;
    let numSamples = 8;

    if (data[13] < this.currentTemp + delta && data[13] > this.currentTemp - delta) {
      this.tempData = [];
    } else if (data[13] > this.currentTemp + delta && this.tempData.length != 0) {
      if (this.tempData[0] < this.currentTemp + delta) {
        // if previous data set was not above the current, then reset and start over
        this.tempData = [];
      }
      this.tempData.push(data[13]);
    } else if (data[13] < this.currentTemp - delta && this.tempData.length != 0) {
      if (this.tempData[0] > this.currentTemp - delta) {
        // if previous data set was not below the current, then reset and start over
        this.tempData = [];
      }
      this.tempData.push(data[13]);
    } else {
      // otherwise just push the data
      this.tempData.push(data[13]);
    }

    // check if number of samples has been reached
    if (this.tempData.length == numSamples) {
        this.currentTemp = 0;
        for (let i = 0; i < numSamples; i++)
          this.currentTemp += this.tempData[i];
        this.currentTemp /= numSamples;
        this.tempData = [];
        this.tempData.push(data[13]);
    }
  }

  getTemp() {
    return this.currentTemp;
  }
  getMidPoint() {
    return this.result;
  }

  getMax() {
    return this.atMax;
  }
}
var holdingData = new heldData();

function getForceData(data) {
  let min = data[1]
  let max = data[1]
  for(let i = 1; i < 9; i++) {
    min = Math.min(min,data[i])
    max = Math.max(max,data[i])
  }
  // return 0-1, where 1 is the worst possible range.
  return 1-(max-min)/1024
}

function getUniformity(data) {
  let max = 0;
  let load = [0,0,0,0,0,0,0,0];
  // find max load
  for(let i = 1; i < 9; i++) {
    max += data[i];
  }

  // create precentage for each sensor
  for (let i = 1; i < 9; i++) {
    load[i-1] = (data[i]/max) * 100;
    if (max == 0) {
      load[i-1] = 0;
    }
  }
  return load;
}

function toPsi(data) {
  let out = (data[9]/1.8)/51.7;

  return out;
}

function thermistorToF(data) {
  holdingData.addTempData(data);
  let thermistorNominal = 100000;
  let temperatureNominal = 25;
  let seriesResistor = 100000;
  let Bcoefficient = 3950;

  let temperature = holdingData.getTemp();
  // console.log(average);
  temperature = (1023/temperature) - 1;
  temperature = (seriesResistor/temperature);

  let steinhart = temperature / thermistorNominal;
  steinhart = math.log(steinhart);
  steinhart = steinhart / Bcoefficient;
  steinhart += (1/(temperatureNominal + 273.15));
  steinhart = 1/steinhart;
  steinhart -= 273.15;
  steinhart = (steinhart * 9/5) + 32;

  // console.log(steinhart);
  return math.round(steinhart);//((temp * (9/5)) + 32);
  
//  return data[12];
}

function boxMotorOnOff(data) {
  let state = [];
  for (let i = 4; i < data.length; i+=3) {
    state.push((data[i] > data[i+1] ? 1 : 0));
  }

  return state;
}

function getEncoderMidpoint(data) {
  return holdingData.getMidPoint();
}

function getEncoderMax(data) {
  holdingData.addEncoderData(data[13]);
  return holdingData.getMax();
}

module.exports = {
  process(data) {
    return {
      force_data: getForceData(data),
      uniform_data: getUniformity(data),
      psi_data: toPsi(data),
      temp_data: thermistorToF(data),
      box_motor_on_off: boxMotorOnOff(data),
      encoder_midpoint: getEncoderMidpoint(data),
      encoder_max: getEncoderMax(data)
    }
  }
}
