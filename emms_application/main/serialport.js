const SerialPort = require("serialport");
const Delimiter = require("@serialport/parser-delimiter");
const fs = require("fs");
const path = require("path");
var events = require("events");

class Serial {
  // class can be used to read values form serial port
  // will emit events with data and write values to log file
  constructor(portName, baud, fileName, num, dataLen = 36) {
    this.timeStamp = 0;
    this.timeStampSet = false;
    this.dataLen = dataLen;
    this.portName = portName;
    this.baud = baud;
    this.file = fileName;
    this.emitter = new events.EventEmitter();
    this.emitterNum = num;

    this.port = undefined;

    this.enable = this.enable.bind(this);
    this.disable = this.disable.bind(this);
    this.isEnabled = false;
    this.beginRead = false;

    this.emitSerialData = this.emitSerialData.bind(this);
    this.handlePortOpen = this.handlePortOpen.bind(this);
    this.handlePortError = this.handlePortError.bind(this);
    this.handlePortClose = this.handlePortClose.bind(this);
    // this.calibrate = this.calibrate.bind(this);
    // this.calibrationOn = false;
    this.dataOffset = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
    this.scaling    = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1/99, 0.5];
  }

  // Open serial port and set up listeners for relevant events
  enable(args) {
    Object.assign(this,args);
    this.emitter.removeAllListeners();
    this.port = new SerialPort(this.portName, {baudRate: this.baud});
    this.parser = this.port.pipe(new Delimiter({delimiter: "\n\n\n"}));

    // events to listen to
    this.port.once("open", this.handlePortOpen);
    this.parser.on("data", this.emitSerialData);
    this.port.once("error", this.handlePortError);
    this.port.once("close", this.handlePortClose);
    this.isEnabled = true;

    return this.emitter;
  }

  logOpen(fileName) {
    this.file = fileName;
    let dir = path.dirname(this.file);
    if(!dir)
      fs.mkdirSync(dir, { recursive: true });
    this.file = fs.createWriteStream(this.file, {flags: "w"});
    console.log("File open ".concat(this.emitterNum));
  }

  logClose() {
    this.file.close();
    console.log("File closed ".concat(this.emitterNum));
    this.emitter.emit("log_closed_".concat(this.emitterNum), this);
  }

  // Sets time stamp and begins logging data
  setTimeStamp(fileName) {
    if (!this.isEnabled) return;
    this.timeStampSet = true;
    console.log("setTimeStamp ".concat(this.emitterNum));
    this.logOpen(fileName);
  }

  endTimeStamp() {
    if (!this.timeStampSet) return;
    this.timeStampSet = false;
    this.timeStamp = 0;
    console.log("endTimeStamp ".concat(this.emitterNum));
    this.logClose();
  }

  // close port and file stream
  disable() {
    if (this.isEnabled) {
      this.port.close();
      this.isEnabled = false;
    }
  }

  calibrate(data) {
    // TODO: send device calibration values
    this.port.write('c');
  }
  changeBaud(baud) {
    // ` is ascii for 96
    // s is ascii for 115
    if (baud == 9600) {
      this.port.write('`');
    } else {
      this.port.write('s');
    }

    // Opening and closing port will reset arduino
    this.port.close();
    this.port.open();
  }

  changeSensors(numSensors) {
    if (numSensors == 4) {
      this.port.write('4');
    } else {
      this.port.write('8');
    }

    // Opening and closing port will reset arduino
    this.port.close();
    this.port.open();
  }
  // promise to return object array of available ports
  static portList() {
    // returns object like
    //{
    //  comName: "/dev/tty.usbmodem1421",
    //  manufacturer: "Arduino (www.arduino.cc)",
    //  serialNumber: "752303138333518011C1",
    //  pnpId: undefined,
    //  locationId: "14500000",
    //  productId: "0043",
    //  vendorId: "2341"
    //}
    return SerialPort.list();
  }

  // send data with "data" event. Also writes to CSV log file
  emitSerialData(data) {
    if (this.timeStampSet == false) {
      if (data.length < 4) return; // don't bother reading if too small
      this.tempTimeStamp = data.readUInt32LE(0);
      return; // Leave if time stamp isn't set
    } else {
      if (this.timeStamp == 0) {
        this.timeStamp = data.readUInt32LE(0);
      }
    }
    if (data.length === this.dataLen) {
      // reinitialize timestamp if it causes negative values
      if ((data.readUInt32LE(0) - this.timeStamp) < 0) {
        this.timeStamp = data.readUInt32LE(0);
      }
      // console.log(data);
      let dataArr = [
        // Time
        data.readUInt32LE(0) - this.timeStamp,
        // Force Sensors
        data.readUInt16LE(4)*this.scaling[0],
        data.readUInt16LE(6)*this.scaling[1],
        data.readUInt16LE(8)*this.scaling[2],
        data.readUInt16LE(10)*this.scaling[3],
        data.readUInt16LE(12)*this.scaling[4],
        data.readUInt16LE(14)*this.scaling[5],
        data.readUInt16LE(16)*this.scaling[6],
        data.readUInt16LE(18)*this.scaling[7],
        // PSI
        data.readUInt16LE(20)*this.scaling[8],
        // Flow Rate
        data.readUInt16LE(22)*this.scaling[9], // 10
        // Motor Current
        data.readUInt16LE(24)*this.scaling[10], // 11
        // Motor Voltage
        data.readUInt16LE(26)*this.scaling[11], // 12
        // Motor Temp
        data.readUInt16LE(28)*this.scaling[12], //13
        // Encoder Position
        data.readInt32LE(30)*this.scaling[13],
        // Strap Constriction - same as above, different scale
        (data.readInt32LE(30)*this.scaling[14]).toFixed(2),
        // Estim at Skin
        (data.readUInt16LE(34)*this.scaling[15]).toFixed(2)
        // Compression and Estim
        // Add Compression and Estim
      ];
      // console.log(dataArr);
      this.emitter.emit("data_".concat(this.emitterNum), dataArr);
      this.file.write(dataArr.join(","));
      this.file.write("\n");
    } else {
      console.error("Data of wrong length, got " + data.length + ", expected " + this.dataLen);
      return;
    }
  }

  handleCycleData(data) {
    for(var i = 0; i < data.length; i++) {
      // console.log(data[i]);
      // console.log(data.data[i]);
    }
  }

  handlePortOpen() {
    console.log("port_open_".concat(this.emitterNum));
    this.emitter.emit("serial_open_".concat(this.emitterNum));
    this.isEnabled = true;
    this.timeStampSet = false;
  }

  handlePortClose() {
    console.log("port_closed_".concat(this.emitterNum));
    this.emitter.emit("serial_closed_".concat(this.emitterNum),this);
    this.timeStamp = 0;
    this.timeStampSet = false;
    this.isEnabled = false;
  }

  handlePortError(err) {
    this.emitter.emit("serial_error_".concat(this.emitterNum), err);
    this.emitter.removeAllListeners();
    this.timeStamp = 0;
    this.timeStampSet = false;
    this.isEnabled = false;
  }
}

module.exports = Serial;
