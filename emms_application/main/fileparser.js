const fs = require('fs');
const readline = require('readline');
var events = require("events");

class FileParser {
  // class can be used to read from CSV file
  constructor(fileName, emitter) {
    this.rl = readline.createInterface({
      input: fs.createReadStream(fileName),
      crlfDelay: Infinity
    });
    this.emitter = emitter;

    this.enable = this.enable.bind(this);
  }

  enable() {
    this.rl.on("line", line => {
      var array = line.split(",").map(el => parseFloat(el));
      //console.log(array);
      this.emitter.emit("data", array);
    });
    this.rl.on('close', () => {
      this.emitter.emit('close');
    })
  }

}

module.exports = FileParser;
