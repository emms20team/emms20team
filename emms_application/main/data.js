var test = [
  [0,0,0,0,0,0,0, 2.3],
  [0,0,0,0,0,0,0, 2.3],
  [0,0,0,0,0,0,0, 2.3],
  [0,0,0,0,0,0,0, 2.4],
  [0,0,0,0,0,0,0, 2.5],
  [0,0,0,0,0,0,0, 3.3],
  [0,0,0,0,0,0,0, 4.3],
  [0,0,0,0,0,0,0, 5.3],
  [0,0,0,0,0,0,0, 4.3],
  [0,0,0,0,0,0,0, 3.3],
  [0,0,0,0,0,0,0, 2.3],
  [0,0,0,0,0,0,0, 1.3],
  [0,0,0,0,0,0,0, 0.3],
  [0,0,0,0,0,0,0, 1.3],
  [0,0,0,0,0,0,0, 2.3],


  [0,0,0,0,0,0,0, 2.3],
  [0,0,0,0,0,0,0, 2.3],
  [0,0,0,0,0,0,0, 2.3],
  [0,0,0,0,0,0,0, 2.4],
  [0,0,0,0,0,0,0, 2.5],
  [0,0,0,0,0,0,0, 3.3],
  [0,0,0,0,0,0,0, 4.3],
  [0,0,0,0,0,0,0, 5.3],
  [0,0,0,0,0,0,0, 4.3],
  [0,0,0,0,0,0,0, 3.3],
  [0,0,0,0,0,0,0, 2.3],
  [0,0,0,0,0,0,0, 1.3],
  [0,0,0,0,0,0,0, 0.3],
  [0,0,0,0,0,0,0, 1.3],
  [0,0,0,0,0,0,0, 2.3],


  [0,0,0,0,0,0,0, 2.3],
  [0,0,0,0,0,0,0, 2.3],
  [0,0,0,0,0,0,0, 2.3],
  [0,0,0,0,0,0,0, 2.4],
  [0,0,0,0,0,0,0, 2.5],
  [0,0,0,0,0,0,0, 3.3],
  [0,0,0,0,0,0,0, 4.3],
  [0,0,0,0,0,0,0, 5.3],
  [0,0,0,0,0,0,0, 4.3],
  [0,0,0,0,0,0,0, 3.3],
  [0,0,0,0,0,0,0, 2.3],
  [0,0,0,0,0,0,0, 1.3],
  [0,0,0,0,0,0,0, 0.3],
  [0,0,0,0,0,0,0, 1.3],
  [0,0,0,0,0,0,0, 2.3],


  [0,0,0,0,0,0,0, 2.3],
  [0,0,0,0,0,0,0, 2.3],
  [0,0,0,0,0,0,0, 2.3],
  [0,0,0,0,0,0,0, 2.4],
  [0,0,0,0,0,0,0, 2.5],
  [0,0,0,0,0,0,0, 3.3],
  [0,0,0,0,0,0,0, 4.3],
  [0,0,0,0,0,0,0, 5.3],
  [0,0,0,0,0,0,0, 4.3],
  [0,0,0,0,0,0,0, 3.3],
  [0,0,0,0,0,0,0, 2.3],
  [0,0,0,0,0,0,0, 1.3],
  [0,0,0,0,0,0,0, 0.3],
  [0,0,0,0,0,0,0, 1.3],
  [0,0,0,0,0,0,0, 2.3],


  [0,0,0,0,0,0,0, 2.3],
  [0,0,0,0,0,0,0, 2.3],
  [0,0,0,0,0,0,0, 2.3],
  [0,0,0,0,0,0,0, 2.4],
  [0,0,0,0,0,0,0, 2.5],
  [0,0,0,0,0,0,0, 3.3],
  [0,0,0,0,0,0,0, 4.3],
  [0,0,0,0,0,0,0, 6.3],
  [0,0,0,0,0,0,0, 4.3],
  [0,0,0,0,0,0,0, 3.3],
  [0,0,0,0,0,0,0, 2.3],
  [0,0,0,0,0,0,0, 1.3],
  [0,0,0,0,0,0,0, 0.1],
  [0,0,0,0,0,0,0, 0.3],
  [0,0,0,0,0,0,0, 1.3],
  [0,0,0,0,0,0,0, 2.3],
];
const fs = require("fs");

// TODO: should have a version that reads form file
class Data {
  constructor(epsilon=1, psIdx=7) {
    this._epsilon = epsilon;
    this._psIdx = psIdx;

    this._data = [];

    this._occurence = {};
  }

  addData(data) {
    // TODO: i baseline needs to be the lowest value that occurs the most
    this._data.push(data[this._psIdx]);

    let psVal = Math.ceil(data[this._psIdx]);

    if (this._occurence[psVal] === undefined) {
      this._occurence[psVal] = 1;
    }
    else {
      this._occurence[psVal] += 1;
    }
  }

  getAnalysis() {
    // TODO: base + epsilon to detect when cycle starts?
    // TODO: base - epsilon to detect when valley starts?
    let baseline = this.getMode();

    let inCycle = false;
    let cycleNum = 0;
    let cycleArea = 0;
    let maxPeak = 0;
    let maxPeakSum = 0;

    let inValley = false;
    let valleyNum = 0;
    let minValley = baseline;
    let minValleySum = 0;

    let currentPeak = 0;
    let currentArea = 0;
    let currentValley = baseline;

    for (let i = 0; i < this._data.length; i++) {
      let data = this._data[i];

      // gather peak data
      if (inCycle) {
        // cycle ends
        if (data < baseline + this._epsilon) {
          cycleNum++;
          cycleArea += currentArea;
          maxPeakSum += currentPeak;
          maxPeak = (currentPeak > maxPeak) ? currentPeak : maxPeak;

          currentArea = 0;
          currentPeak = 0;
          inCycle = false;
        }
        // in cycle handle current data
        else {
          currentArea += data;

          if (data > currentPeak) {
            currentPeak = data;
          }
        }
      }

      // gather valley data
      else if (inValley) {
        if (data > (baseline - this._epsilon)) {
          valleyNum++;
          minValleySum += currentValley;
          minValley = (currentValley < minValley) ? currentValley : minValley;

          currentValley = baseline;
          inValley = false;
        }
        else {
          if (data < currentValley) {
            currentValley = data;
          }
        }
      }

      else if (data > baseline + this._epsilon) {
        inCycle = true;
        currentArea += data;
        currentPeak = data;
      }

      else if (data < (baseline - this._epsilon)) {
        inValley = true
        currentValley = data;
      }
    }

    return {
      cycleNum: cycleNum,
      cycleAvgArea: (cycleArea / cycleNum),
      peakMax: maxPeak,
      peakAvg: (maxPeakSum / cycleNum),
      valleyNum: valleyNum,
      valleyMin: minValley,
      valleyAvg: (minValleySum / valleyNum),
    };
  }

  getMode() {
    return Number(Object.keys(this._occurence).reduce(
      (a, b) => this._occurence[a] > this._occurence[b] ? a : b
    ));
  }
}

module.exports = Data;

let data = new Data();

for (let i = 0; i < test.length; i++) {
  data.addData(test[i]);
}

console.log(data.getAnalysis());
