// This is code that's ran on the renderer process, so any heavy code shouldn't be here! It'll lag the interface!
const {ipcRenderer} = require('electron')
const chart_utils = require('./chart_utils.js')
const {DataFile} = require('../main/dataFile.js')
const DataAnalysis = require('../main/data_analysis.js');
const DataModal = require("./dataModal.js");

const fill = document.querySelector('.fill');
const empties = document.querySelectorAll('.empty');

function dragStart() {
  console.log('start');
}

function dragEnd() {
  console.log('end');
}

let history = [];
let history2 = [];
let history_index = 0;
let fileName;

function update_history(
  history_index,
  load_chart,
  uniform_chart,
  chart_master,
) {

  load_chart.data.datasets[0] = {};
  uniform_chart.data.datasets[0] = {};

  let history_max = history_index + 512;
  for(let j = history_index; j < history_max; j++)
    if(history[j]) {
      chart_utils.set_data(
        load_chart,
        uniform_chart,
        chart_master,
        history[j],
        DataAnalysis.process(history[j]),
      );
    }
}

let zones = {};
let zoneKeys = []; // the names of each log file
let zone_index = 0;

function update_zone_history(
  zone_history_index,
  chart_zone,
  chart_point,
) {
  let history_max = zone_history_index + 512;
  zoneKeys.forEach((key, idx) => {
    for(let j = zone_history_index; j < history_max; j++) {
      if(zones[key][j]) {
        chart_utils.set_zones_data(
          idx,
          chart_zone,
          zones[key][j],
          DataAnalysis.process(zones[key][j]),
          chart_point,
        );
      }
    }
  });
}

// wait for load
$(() => {
  let $history_position = $('#history-position');
  let $zone_position = $('#zone-position');

  let chart_load = chart_utils.make_chart('chart-force', 0);
  let chart_uniform = chart_utils.make_chart('chart-uniform', 1);
  let chart_master = chart_utils.make_chart('chart-master', 6);
  let chart_zones = chart_utils.make_chart('chart-zones', 4);

  let $cycle_list = $('#cycle_list');

  /******************************************************
   * LOG FILES
  ******************************************************/
  // TODO: look at todo in log_close event listener
  // TODO: on click for zone graph sliders
  // TODO: on change for drop down menu update_zone_graph
  // ZONES

  const zoneTabClick = e => {
    history = zones[e.target.id]
    history_index = parseInt($("#history-position").val());
    fileName = e.target.id;
    update_history(
      history_index,
      chart_load,
      chart_uniform,
      chart_master,
    );
    document.getElementById("chart-serial-temperature")
      .innerHTML = `${DataAnalysis.process(history[history_index]).temp_data}*F`;
  }

  ipcRenderer.on("zone_data", (sender, data, path) => {
    if (path in zones) {
      zones[path].push(data);
    }
    else {
      zones[path] = [data];
    }
  });

  ipcRenderer.on('zones_close', (sender, path) => {
    // update comparison chart
    let min = Infinity;
    zoneKeys = Object.keys(zones);
    zoneKeys.forEach((key, idx) => {
      if (key.slice(-5, -4) !== "X") {
        min = min > zones[key].length ? zones[key].length : min
      }
    })
    $zone_position.attr('max', min);
    update_zone_history(0, chart_zones, $("#zones_chart_point").val());

    // update main chart
    fileName = path;
    $history_position.attr('max', min);
    history = zones[zoneKeys[0]];
    history_index = 0;
    let $zoneTabs = $("#zone-tabs");
    zoneKeys.forEach((key, idx) => {
      let $zoneTab = $(
        `<li><a href=# id="${key}">Zone ${key.slice(-5,-4)}</a></li>`
      );
      $zoneTab.click(zoneTabClick);
      $zoneTabs.append($zoneTab);
    });
    update_history(
      0,
      chart_load,
      chart_uniform,
      chart_master,
    );
    document.getElementById("chart-serial-temperature")
      .innerHTML = `${DataAnalysis.process(history[0]).temp_data}*F`;
  })

  let last_zone_index = -1;

  $zone_position.on('change mousemove', function(e) {
    zone_index = parseInt($(this).val());
    if(last_zone_index != zone_index) {
      last_zone_index = zone_index;
      update_zone_history(
        zone_index,
        chart_zones,
        $("#zones_chart_point").val()
      );
    }
  })

  let last_index = -1;

  // receive data here
  $history_position.on('change mousemove', function(e) {
    history_index = parseInt($(this).val());
    if(last_index != history_index) {
      last_index = history_index;
      document.getElementById("chart-serial-temperature")
        .innerHTML = `${DataAnalysis.process(history[history_index]).temp_data}*F`;

      update_history(
        history_index,
        chart_load,
        chart_uniform,
        chart_master,
      );
    }
  })

  // Beginning cycle data calculation
  var cycles = [];
  var cycle_start = 0;
  var cycle_duration = 0;
  var cycle_count = 0;
  let $cycle_duration = $('#cycle_duration');
  let $cycle_units = $('#cycle_units');
  let $cycle_set = $('#cycle_set');

  // end cycle
  $cycle_list.on('click', (e) => end_cycle(e.target.name));

  function end_cycle(index) {
    let cycleDatas = {};
    zoneKeys.forEach((k, i) => {
      let df = new DataFile(k, cycles[index][0], cycles[index][1]);
      cycleDatas[k] = df.run();
    });
    DataModal.loadDataModal(cycleDatas, zoneKeys);
  }

  // set cycle
  $cycle_set.on('click', set_cycle);

  function set_cycle() {
    cycle_start = history[history_index][0];
    cycle_duration = $cycle_duration.val() * $cycle_units.val();
    cycles.push([cycle_start, cycle_duration]);
    // this will append cycle_count for each cycle as its 'name' attr, which will be used later as the index for the selected cycle data
    $cycle_list.append($(`<li><a href="#" name="${cycle_count}">` + "Cycle: " + (cycle_count + 1) + '</a></li>'));
    cycle_count++;
  }

  ipcRenderer.send('page_ready');

  chart_utils.start_draw_cycle([
    chart_load,
    chart_uniform,
    chart_master,
    chart_zones,
  ]);

  // zone chart y axis stuff
  // zones chart
  let $zone_1_scale_min = $('#zone_1_scale_min');
  let $zone_1_scale_max = $('#zone_1_scale_max');
  let $zone_2_scale_min = $('#zone_2_scale_min');
  let $zone_2_scale_max = $('#zone_2_scale_max');
  let $zone_3_scale_min = $('#zone_3_scale_min');
  let $zone_3_scale_max = $('#zone_3_scale_max');
  let $zone_4_scale_min = $('#zone_4_scale_min');
  let $zone_4_scale_max = $('#zone_4_scale_max');

  let $zones_chart_point = $('#zones_chart_point');
  let zones_min = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
  let zones_max = [
    6, 1000, 1000, 500,
    200, 500, 500, 500,
    500, 50, 50, 50, 50, 50
  ];

  $zones_chart_point.on('change', zones_chart_change)

  function zones_chart_change () {
      $zone_1_scale_min.attr('min', zones_min[$zones_chart_point.val()]);
      $zone_1_scale_max.attr('max', zones_max[$zones_chart_point.val()]);
      $zone_2_scale_min.attr('min', zones_min[$zones_chart_point.val()]);
      $zone_2_scale_max.attr('max', zones_max[$zones_chart_point.val()]);
      $zone_3_scale_min.attr('min', zones_min[$zones_chart_point.val()]);
      $zone_3_scale_max.attr('max', zones_max[$zones_chart_point.val()]);
      $zone_4_scale_min.attr('min', zones_min[$zones_chart_point.val()]);
      $zone_4_scale_max.attr('max', zones_max[$zones_chart_point.val()]);
  }

  let $zones_chart_yaxes = $('#zones_chart_yaxes');
  $zones_chart_yaxes.on('click', zones_chart_yaxes);

  let toggleBtn = false;
  function zones_chart_yaxes () {
    if(!toggleBtn) {
      toggleBtn = true;
      for(let i = 0; i < 4; i++) {
        chart_zones.options.scales.yAxes[i].display = true;
      }
      document.getElementById('zones_chart_slider').style.display = 'block';
    } else {
      toggleBtn = false;
      for(let i = 0; i < 4; i++) {
        chart_zones.options.scales.yAxes[i].display = false;
      }
      document.getElementById('zones_chart_slider').style.display = 'none';
    }
  }

  // radar charts
  let $force_scale_max = $('#force_scale_max');
  let $uniform_scale_max = $('#uniform_scale_max');

  // master chart
  let $psi_scale_min = $('#psi_scale_min');
  let $psi_scale_max = $('#psi_scale_max');
  let $flow_rate_scale_min = $('#flow_rate_scale_min');
  let $flow_rate_scale_max = $('#flow_rate_scale_max');
  let $full_current_scale_min = $('#full_current_scale_min');
  let $full_current_scale_max = $('#full_current_scale_max');
  let $full_voltage_scale_min = $('#full_voltage_scale_min');
  let $full_voltage_scale_max = $('#full_voltage_scale_max');
  let $motor_current_scale_min = $('#motor_current_scale_min');
  let $motor_current_scale_max = $('#motor_current_scale_max');
  let $motor_voltage_scale_min = $('#motor_voltage_scale_min');
  let $motor_voltage_scale_max = $('#motor_voltage_scale_max');
  let $motor_temp_scale_min = $('#motor_temp_scale_min');
  let $motor_temp_scale_max = $('#motor_temp_scale_max');
  let $encoder_position_scale_min = $('#encoder_position_scale_min');
  let $encoder_position_scale_max = $('#encoder_position_scale_max');
  let $strap_constriction_scale_min = $('#strap_constriction_scale_min');
  let $strap_constriction_scale_max = $('#strap_constriction_scale_max');
  let $estim_skin_scale_min = $('#estim_skin_scale_min');
  let $estim_skin_scale_max = $('#estim_skin_scale_max');
  let $sync_scale_min = $('#sync_scale_min');
  let $sync_scale_max = $('#sync_scale_max');
  let $motor_state_scale_min = $('#motor_state_scale_min');
  let $motor_state_scale_max = $('#motor_state_scale_max');
  let $box_current_scale_min = $('#box_current_scale_min');
  let $box_current_scale_max = $('#box_current_scale_max');
  let $box_estim_scale_min = $('#box_estim_scale_min');
  let $box_estim_scale_max = $('#box_estim_scale_max');

  function update_line_range() {
    chart_load.options.scale.ticks.max = parseInt($force_scale_max.val());
    chart_uniform.options.scale.ticks.max = parseInt($uniform_scale_max.val());
  }
  $force_scale_max.on('change mousemove', update_line_range);
  $force_scale_max.on('change mousemove', update_line_range);

  const updateViewWidth = (idx, $element, chart, step=1) => {
    let prev = parseInt($element.val());
    let current = prev;
    return () => {
      current = parseInt($element.val());
      let val = (current - prev) / step;
      chart.options.scales.yAxes[idx].ticks.min -= val;
      chart.options.scales.yAxes[idx].ticks.max += val;
      prev = current;
    }
  }

  const updateViewPosition = (idx, $element, chart, step=1) => {
    let prev = parseInt($element.val());
    let current = prev;
    return () => {
      current = parseInt($element.val());
      let val = (prev - current) / step;
      chart.options.scales.yAxes[idx].ticks.min += val;
      chart.options.scales.yAxes[idx].ticks.max += val;
      prev = current;
    }
  }

  // slider events -- update scale when value of slider changes
  $psi_scale_min.on('change mousemove', updateViewWidth(0, $psi_scale_min, chart_master, 50));
  $psi_scale_max.on('change mousemove', updateViewPosition(0, $psi_scale_max, chart_master, 50));
  $flow_rate_scale_min.on('change mousemove', updateViewWidth(1, $flow_rate_scale_min, chart_master, 50));
  $flow_rate_scale_max.on('change mousemove', updateViewPosition(1, $flow_rate_scale_max, chart_master, 50));
  $full_current_scale_min.on('change mousemove', updateViewWidth(2, $full_current_scale_min, chart_master));
  $full_current_scale_max.on('change mousemove', updateViewPosition(2, $full_current_scale_max, chart_master));
  $full_voltage_scale_min.on('change mousemove', updateViewWidth(3, $full_voltage_scale_min, chart_master));
  $full_voltage_scale_max.on('change mousemove', updateViewPosition(3, $full_voltage_scale_max, chart_master));
  $motor_current_scale_min.on('change mousemove', updateViewWidth(4, $motor_current_scale_min, chart_master));
  $motor_current_scale_max.on('change mousemove', updateViewPosition(4, $motor_current_scale_max, chart_master));
  $motor_voltage_scale_min.on('change mousemove', updateViewWidth(5, $motor_voltage_scale_min, chart_master));
  $motor_voltage_scale_max.on('change mousemove', updateViewPosition(5, $motor_voltage_scale_max, chart_master));
  $motor_temp_scale_min.on('change mousemove', updateViewWidth(6, $motor_temp_scale_min, chart_master));
  $motor_temp_scale_max.on('change mousemove', updateViewPosition(6, $motor_temp_scale_max, chart_master));
  $encoder_position_scale_min.on('change mousemove', updateViewWidth(7, $encoder_position_scale_min, chart_master, 0.5));
  $encoder_position_scale_max.on('change mousemove', updateViewPosition(7, $encoder_position_scale_max, chart_master, 0.5));
  $strap_constriction_scale_min.on('change mousemove', updateViewWidth(8, $strap_constriction_scale_min, chart_master));
  $strap_constriction_scale_max.on('change mousemove', updateViewPosition(8, $strap_constriction_scale_max, chart_master));
  $estim_skin_scale_min.on('change mousemove', updateViewWidth(9, $estim_skin_scale_min, chart_master));
  $estim_skin_scale_max.on('change mousemove', updateViewPosition(9, $estim_skin_scale_max, chart_master));
  $sync_scale_min.on('change mousemove', updateViewWidth(10, $sync_scale_min, chart_master));
  $sync_scale_max.on('change mousemove', updateViewPosition(10, $sync_scale_max, chart_master));
  $motor_state_scale_min.on('change mousemove', updateViewWidth(11, $motor_state_scale_min, chart_master));
  $motor_state_scale_max.on('change mousemove', updateViewPosition(11, $motor_state_scale_max, chart_master));
  $box_current_scale_min.on('change mousemove', updateViewWidth(12, $box_current_scale_min, chart_master));
  $box_current_scale_max.on('change mousemove', updateViewPosition(12, $box_current_scale_max, chart_master));
  $box_estim_scale_min.on('change mousemove', updateViewWidth(13, $box_estim_scale_min, chart_master));
  $box_estim_scale_max.on('change mousemove', updateViewPosition(13, $box_estim_scale_max, chart_master));

  $zone_1_scale_min.on('change mousemove', updateViewWidth(0, $zone_1_scale_min, chart_zones));
  $zone_1_scale_max.on('change mousemove', updateViewPosition(0, $zone_1_scale_max, chart_zones));
  $zone_2_scale_min.on('change mousemove', updateViewWidth(1, $zone_2_scale_min, chart_zones));
  $zone_2_scale_max.on('change mousemove', updateViewPosition(1, $zone_2_scale_max, chart_zones));
  $zone_3_scale_min.on('change mousemove', updateViewWidth(2, $zone_3_scale_min, chart_zones));
  $zone_3_scale_max.on('change mousemove', updateViewPosition(2, $zone_3_scale_max, chart_zones));
  $zone_4_scale_min.on('change mousemove', updateViewWidth(3, $zone_4_scale_min, chart_zones));
  $zone_4_scale_max.on('change mousemove', updateViewPosition(3, $zone_4_scale_max, chart_zones));

  // Master chart checkbox logic
  let $y_axes_checkbox = $('#y_axes_checkbox');
  let $psi_checkbox = $('#psi_checkbox');
  let $flow_rate_checkbox = $('#flow_rate_checkbox');
  let $full_current_checkbox = $('#full_current_checkbox');
  let $full_voltage_checkbox = $('#full_voltage_checkbox');
  let $motor_current_checkbox = $('#motor_current_checkbox');
  let $motor_voltage_checkbox = $('#motor_voltage_checkbox');
  let $motor_temp_checkbox = $('#motor_temp_checkbox');
  let $encoder_position_checkbox = $('#encoder_position_checkbox');
  let $strap_constriction_checkbox = $('#strap_constriction_checkbox');
  let $estim_skin_checkbox = $('#estim_skin_checkbox');
  let $sync_checkbox = $('#sync_checkbox');
  let $motor_state_checkbox = $('#motor_state_checkbox');
  let $box_current_checkbox = $('#box_current_checkbox');
  let $box_estim_checkbox = $('#box_estim_checkbox');

  $y_axes_checkbox.on('click', renderMultiChart);
  $psi_checkbox.on('click', renderMultiChart);
  $flow_rate_checkbox.on('click', renderMultiChart); $full_current_checkbox.on('click', renderMultiChart);
  $full_voltage_checkbox.on('click', renderMultiChart);
  $motor_current_checkbox.on('click', renderMultiChart);
  $motor_voltage_checkbox.on('click', renderMultiChart);
  $motor_temp_checkbox.on('click', renderMultiChart);
  $encoder_position_checkbox.on('click', renderMultiChart);
  $strap_constriction_checkbox.on('click', renderMultiChart);
  $estim_skin_checkbox.on('click', renderMultiChart);
  $sync_checkbox.on('click', renderMultiChart);
  $motor_state_checkbox.on('click', renderMultiChart);
  $box_current_checkbox.on('click', renderMultiChart);
  $box_estim_checkbox.on('click', renderMultiChart);

  function renderMultiChart() {
    if(document.getElementById('y_axes_checkbox').checked == true) {
      for(let i = 0; i < 14; i++) {
       //chart_master.options.scales.yAxes[i].display = true;
       document.getElementById('master_chart_slider').style.display = 'block';
       document.getElementById('master_chart_slider').style.width = '40%';
       document.getElementById('chart-master').style.width = '60%';
      }
    } else {
      for(let i = 0; i < 14; i++) {
        chart_master.options.scales.yAxes[i].display = false;
        document.getElementById('master_chart_slider').style.display = 'none';
        document.getElementById('chart-master').style.width = '100%';
      }
    }
    if(document.getElementById('psi_checkbox').checked == true) {
      chart_master.data.datasets[0].borderColor = '#FF8A80';
    } else {
      chart_master.data.datasets[0].borderColor = 'rgba(0,0,0,0)';
    }
    if(document.getElementById('flow_rate_checkbox').checked == true) {
      chart_master.data.datasets[1].borderColor = '#FF80AB';
    } else {
      chart_master.data.datasets[1].borderColor = 'rgba(0,0,0,0)';
    }
    if(document.getElementById('full_current_checkbox').checked == true) {
      chart_master.data.datasets[2].borderColor = '#AED581';
    } else {
      chart_master.data.datasets[2].borderColor = 'rgba(0,0,0,0)';
    }
    if(document.getElementById('full_voltage_checkbox').checked == true) {
      chart_master.data.datasets[3].borderColor = '#48be65';
    } else {
      chart_master.data.datasets[3].borderColor = 'rgba(0,0,0,0)';
    }
    if(document.getElementById('motor_current_checkbox').checked == true) {
      chart_master.data.datasets[4].borderColor = '#EA80FC';
    } else {
      chart_master.data.datasets[4].borderColor = 'rgba(0,0,0,0)';
    }
    if(document.getElementById('motor_voltage_checkbox').checked == true) {
      chart_master.data.datasets[5].borderColor = '#B388FF';
    } else {
      chart_master.data.datasets[5].borderColor = 'rgba(0,0,0,0)';
    }
    if(document.getElementById('motor_temp_checkbox').checked == true) {
      chart_master.data.datasets[6].borderColor = '#8C9EFF';
    } else {
      chart_master.data.datasets[6].borderColor = 'rgba(0,0,0,0)';
    }
    if(document.getElementById('encoder_position_checkbox').checked == true) {
      chart_master.data.datasets[7].borderColor = '#80D8FF';
    } else {
      chart_master.data.datasets[7].borderColor = 'rgba(0,0,0,0)';
    }
    if(document.getElementById('strap_constriction_checkbox').checked == true) {
      chart_master.data.datasets[8].borderColor = '#4DB6AC';
    } else {
      chart_master.data.datasets[8].borderColor = 'rgba(0,0,0,0)';
    }
    if(document.getElementById('estim_skin_checkbox').checked == true) {
      chart_master.data.datasets[9].borderColor = '#fa2020';
    } else {
      chart_master.data.datasets[9].borderColor = 'rgba(0,0,0,0)';
    }
    if(document.getElementById('sync_checkbox').checked == true) {
      chart_master.data.datasets[10].borderColor = '#FFB74D';
    } else {
      chart_master.data.datasets[10].borderColor = 'rgba(0,0,0,0)';
    }
    if(document.getElementById('motor_state_checkbox').checked == true) {
      chart_master.data.datasets[11].borderColor = 'rgb(108, 158, 51)';
    } else {
      chart_master.data.datasets[11].borderColor = 'rgba(0,0,0,0)';
    }
    if(document.getElementById('box_current_checkbox').checked == true) {
      chart_master.data.datasets[12].borderColor = 'rgb(149, 179, 27)';
    } else {
      chart_master.data.datasets[12].borderColor = 'rgba(0,0,0,0)';
    }
    if(document.getElementById('box_estim_checkbox').checked == true) {
      chart_master.data.datasets[13].borderColor = 'rgb(182, 179, 36)';
    } else {
      chart_master.data.datasets[13].borderColor = 'rgba(0,0,0,0)';
    }
  }
});
