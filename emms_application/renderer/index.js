// This is code that's ran on the renderer process, so any heavy code shouldn't be here! It'll lag the interface!
const {ipcRenderer} = require('electron')
const path = require('path');
const chart_utils = require('./chart_utils.js')
const {PORTS} = require("../constants");

String.prototype.replaceAll = function(search, replacement) {
    var target = this;
    return target.replace(new RegExp(search, 'g'), replacement);
};

// wait for load
$(() => {
  let $serial_ports = $('#serial_ports');
  let $serial_ports_2 = $('#serial_ports_2');
  let $serial_ports_3 = $('#serial_ports_3');
  let $serial_ports_4 = $('#serial_ports_4');
  let $serial_box_ports = $('#serial_box_ports');

  let $disable_serial_button = $('#disable_serial');
  let $disable_serial_button_2 = $('#disable_serial_2');
  let $disable_serial_button_3 = $('#disable_serial_3');
  let $disable_serial_button_4 = $('#disable_serial_4');
  let $disable_serial_box_button = $('#disable_box_serial');

  let $enable_serial_button = $('#enable_serial_1');
  let $enable_serial_button_2 = $('#enable_serial_2');
  let $enable_serial_button_3 = $('#enable_serial_3');
  let $enable_serial_button_4 = $('#enable_serial_4');
  let $enable_serial_box_button = $('#enable_box_serial');

  let $calibrate_serial_button_3 = $('#calibrate_serial_3');
  let $calibrate_serial_button_4 = $('#calibrate_serial_4');
  let $calibrate_serial_button = $('#calibrate_serial_1');
  let $calibrate_serial_button_2 = $('#calibrate_serial_2');
  let $refreshSerialPorts = $("#refresh-ports-btn");

  // TODO: make this look better
  let $logBtn = $("#log-btn");
  let $logFileList = $("#log-file-list");
  $logBtn.click(() => {
    if ($logFileList.css("display") === "none") {
      $logFileList.css("display", "block");
    }
    else {
      $logFileList.css("display", "none");
    }
  });

  let $enable_logging = $('#begin_logging');
  let $disable_logging = $('#end_logging');
  let $pause_graph_button = $('#pause_graph_button');
  let $unpause_graph_button = $('#unpause_graph_button');
  let $use_default_logname = $('#log_name_default');
  let $log_name = $('#log_name');
  let $log_list = $('#log_list');
  // make a chart
  let chart_force = chart_utils.make_chart('chart-force', 0);
  let chart_uniform = chart_utils.make_chart('chart-uniform', 1);
  let chart_master = chart_utils.make_chart('chart-master', 6);
  let chart_zones = chart_utils.make_chart('chart-zones', 4);

  // disable these buttons until they're available.
  $disable_serial_button.attr('disabled', true);
  $disable_serial_button_2.attr('disabled', true);
  $enable_serial_button.attr('disabled', true);
  $enable_serial_button_2.attr('disabled', true);
  $calibrate_serial_button.attr('disabled', true);
  $calibrate_serial_button_2.attr('disabled', true);
  $pause_graph_button.attr('disabled', true);
  $unpause_graph_button.attr('disabled', true);

  // added
  $disable_serial_button_3.attr('disabled', true);
  $disable_serial_button_4.attr('disabled', true);
  $enable_serial_button_3.attr('disabled', true);
  $enable_serial_button_4.attr('disabled', true);
  $calibrate_serial_button_3.attr('disabled', true);
  $calibrate_serial_button_4.attr('disabled', true);

  $enable_serial_box_button.attr('disabled', true);
  $disable_serial_box_button.attr('disabled', true);
  $disable_logging.attr('disabled', true);

  // get a list of logfiles
  let logs = ipcRenderer.sendSync('get_logs');
  for(i in logs) {
    $log_list.prepend($("<li><a href='#'>" + logs[i] + "</a></li>"));
  }

  $log_list.on('click', (e) => ipcRenderer.send("open_log", e.target.innerText))
  let chartUpdater = chart_utils.start_draw_cycle([
    chart_force,
    chart_uniform,
    chart_master,
    chart_zones
  ])

  /***********************************************
   * ZONES + BOX - PORTS
   ***********************************************/
  $enable_serial_button.attr('disabled', false);
  $enable_serial_button_2.attr('disabled', false);
  $enable_serial_button_3.attr('disabled', false);
  $enable_serial_button_4.attr('disabled', false);
  $enable_serial_box_button.attr('disabled', false);

  function refreshSerialPorts() {
    $serial_ports.empty();
    $serial_ports_2.empty();
    $serial_ports_3.empty();
    $serial_ports_4.empty();
    $serial_box_ports.empty();

    $serial_ports.append($('<option disabled="disabled" selected="selected">Serial Port</option>'));
    $serial_ports_2.append($('<option disabled="disabled" selected="selected">Serial Port 2</option>'));
    $serial_ports_3.append($('<option disabled="disabled" selected="selected">Serial Port 3</option>'));
    $serial_ports_4.append($('<option disabled="disabled" selected="selected">Serial Port 4</option>'));
    $serial_box_ports.append($('<option disabled="disabled" selected="selected">Serial Box Port</option>'));

    ipcRenderer.send("refresh_serial_ports");
  }
  $refreshSerialPorts.click(refreshSerialPorts);

  ipcRenderer.send('get_serial_ports');
  ipcRenderer.on('serial_ports', (sender, ports) => {
    length = $serial_ports.length;
    for(i in ports){
      $serial_ports.append($("<option value='" + ports[i].comName + "'>" + ports[i].comName + "</option>"));
    }
    console.trace($serial_ports);
  });

  ipcRenderer.send('get_serial_ports_2');
  ipcRenderer.on('serial_ports_2', (sender, ports) => {
    for(i in ports) {
      $serial_ports_2.append($("<option value='" + ports[i].comName + "'>" + ports[i].comName + "</option>"));
    }
  });

  // added
  ipcRenderer.send('get_serial_ports_3');
  ipcRenderer.on('serial_ports_3', (sender, ports) => {
    for(i in ports){
      $serial_ports_3.append($("<option value='" + ports[i].comName + "'>" + ports[i].comName + "</option>"));
    }
  });

  ipcRenderer.send('get_serial_ports_4');
  ipcRenderer.on('serial_ports_4', (sender, ports) => {
    for(i in ports) {
      $serial_ports_4.append($("<option value='" + ports[i].comName + "'>" + ports[i].comName + "</option>"));
    }
  });

  ipcRenderer.send('get_serial_box_ports');
  ipcRenderer.on('serial_box_ports', (sender, ports) => {
    for (i in ports) {
      $serial_box_ports.append($("<option value='" + ports[i].comName + "'>" + ports[i].comName + "</option>"));
    }
  });

  /***********************************************
   * ZONES + BOX - LISTENERS
   ***********************************************/
  /***********************************************
  * Serial Error Detection
  ***********************************************/
  let errorDetected = false;
  let zone_1_prev_ts = -Infinity;
  let zone_2_prev_ts = -Infinity;
  let zone_3_prev_ts = -Infinity;
  let zone_4_prev_ts = -Infinity;
  let zone_box_prev_ts = -Infinity;

  let zone_1_interval;
  let zone_2_interval;
  let zone_3_interval;
  let zone_4_interval;
  let zone_box_interval;

  let zone_1_prev = zone_1_prev_ts;
  let zone_2_prev = zone_1_prev_ts;
  let zone_3_prev = zone_1_prev_ts;
  let zone_4_prev = zone_1_prev_ts;
  let zone_box_prev = zone_1_prev_ts;

  function startSerialErrorTracking() {
    errorDetected = false
    zone_1_prev_ts = -Infinity;
    zone_2_prev_ts = -Infinity;
    zone_3_prev_ts = -Infinity;
    zone_4_prev_ts = -Infinity;
    zone_box_prev_ts = -Infinity;

    if ($enable_serial_button.prop("disabled")) {
      zone_1_interval = setInterval(() => {
       if (zone_1_prev === zone_1_prev_ts) {
         console.log("unplug error 1");
         serialErrorDetected();
       }
       else {
         zone_1_prev = zone_1_prev_ts;
       }
      }, 500);
    }

    if ($enable_serial_button_2.attr("disabled")) {
      zone_2_interval = setInterval(() => {
       if (zone_2_prev === zone_2_prev_ts) {
         console.log("unplug error 2");
         serialErrorDetected();
       }
       else {
         zone_2_prev = zone_2_prev_ts;
       }
      }, 500);
    }

    if ($enable_serial_button_3.attr("disabled")) {
      zone_3_interval = setInterval(() => {
       if (zone_3_prev === zone_3_prev_ts) {
         console.log("unplug error 3");
         serialErrorDetected();
       }
       else {
         zone_3_prev = zone_3_prev_ts;
       }
      }, 500);
    }

    if ($enable_serial_button_4.attr("disabled")) {
      zone_4_interval = setInterval(() => {
       if (zone_4_prev === zone_4_prev_ts) {
         console.log("unplug error 4");
         serialErrorDetected();
       }
       else {
         zone_4_prev = zone_4_prev_ts;
       }
      }, 500);
    }

    // TODO: implment box data error detection
    //if ($enable_serial_box_button.attr("disabled")) {
      //zone_box_interval = setInterval(() => {
       //if (zone_box_prev === zone_box_prev_ts) {
         //console.log("unplug error box");
         //serialErrorDetected();
       //}
       //else {
         //zone_box_prev = zone_box_prev_ts;
       //}
      //}, 500);
    //}
  }

  function serialErrorDetected() {
    // TODO: activate close display here
    if (errorDetected) return;
    errorDetected = true;
    disableLogging();
    refreshSerialPorts();
    alert("Serial read error");
  }

  function endSerialErrorTracking() {
    clearInterval(zone_1_interval);
    clearInterval(zone_2_interval);
    clearInterval(zone_3_interval);
    clearInterval(zone_4_interval);
  }

  function isValidData(currData, prevTs) {
   return currData[0][0] >= prevTs;
  }

  // zone data events
  ipcRenderer.on("zone_data_1", (sender, data_raw) => {
    if(isValidData(data_raw, zone_1_prev_ts)) {
      chart_utils.set_zones_data(0, chart_zones, data_raw[0], data_raw[1], $('#zones_chart_point').val());
      zone_1_prev_ts = data_raw[0][0];
    }
    else {
      serialErrorDetected();
    }
  });
  ipcRenderer.on("zone_data_2", (sender, data_raw) => {
    if(isValidData(data_raw, zone_2_prev_ts)) {
      chart_utils.set_zones_data(1, chart_zones, data_raw[0], data_raw[1], $('#zones_chart_point').val());
      zone_2_prev_ts = data_raw[0][0];
    }
    else {
      serialErrorDetected();
    }
  });
  ipcRenderer.on("zone_data_3", (sender, data_raw) => {
    if(isValidData(data_raw, zone_3_prev_ts)) {
      chart_utils.set_zones_data(2, chart_zones, data_raw[0], data_raw[1], $('#zones_chart_point').val());
      zone_3_prev_ts = data_raw[0][0];
    }
    else {
      serialErrorDetected();
    }
  });
  ipcRenderer.on("zone_data_4", (sender, data_raw) => {
    if(isValidData(data_raw, zone_4_prev_ts)) {
      chart_utils.set_zones_data(3, chart_zones, data_raw[0], data_raw[1], $('#zones_chart_point').val());
      zone_4_prev_ts = data_raw[0][0];
    }
    else {
      serialErrorDetected();
    }
  });
  ipcRenderer.on("zone_data_box", (sender, data) => {
    chart_utils.set_box_zones(chart_zones, data, $('#zones_chart_point').val());
  });

  // Serial 1 port events
  function serialOpenDisplay($eb, $db, $cb, $p) {
    $eb.attr('disabled', true);
    $db.attr('disabled', false);
    $cb.attr('disabled', false);
    $p.attr('disabled', true);
  }
  function serialCloseDisplay($eb, $db, $cb, $p) {
    $eb.attr('disabled', false);
    $db.attr('disabled', true);
    $cb.attr('disabled', true);
    $p.attr('disabled', false);
  }
  ipcRenderer.on('serial_open_1', (sender, data) => {
    console.log("serial open 1 index");
    serialOpenDisplay(
      $enable_serial_button,
      $disable_serial_button,
      $calibrate_serial_button,
      $serial_ports
    );
  });
  ipcRenderer.on('serial_closed_1', (sender, data) => {
    console.log("serial closed 1 index");
    serialCloseDisplay(
      $enable_serial_button,
      $disable_serial_button,
      $calibrate_serial_button,
      $serial_ports
    );
  });
  ipcRenderer.on('serial_error_1', (sender, data) => {
    console.log("serial error 1 index");
    serialCloseDisplay(
      $enable_serial_button,
      $disable_serial_button,
      $calibrate_serial_button,
      $serial_ports
    );
  });

  // Serial 2 port events
  ipcRenderer.on('serial_open_2', (sender, data) => {
    serialOpenDisplay(
      $enable_serial_button_2,
      $disable_serial_button_2,
      $calibrate_serial_button_2,
      $serial_ports_2
    );
  });
  ipcRenderer.on('serial_closed_2', (sender, data) => {
    serialCloseDisplay(
      $enable_serial_button_2,
      $disable_serial_button_2,
      $calibrate_serial_button_2,
      $serial_ports_2
    );
  });
  ipcRenderer.on('serial_error_2', (sender, data) => {
    serialCloseDisplay(
      $enable_serial_button_2,
      $disable_serial_button_2,
      $calibrate_serial_button_2,
      $serial_ports_2
    );
  });

  // Serial 3 port events
  ipcRenderer.on('serial_open_3', (sender, data) => {
    serialOpenDisplay(
      $enable_serial_button_3,
      $disable_serial_button_3,
      $calibrate_serial_button_3,
      $serial_ports_3
    );
  });
  ipcRenderer.on('serial_closed_3', (sender, data) => {
    serialCloseDisplay(
      $enable_serial_button_3,
      $disable_serial_button_3,
      $calibrate_serial_button_3,
      $serial_ports_3
    );
  });
  ipcRenderer.on('serial_error_3', (sender, data) => {
    serialCloseDisplay(
      $enable_serial_button_3,
      $disable_serial_button_3,
      $calibrate_serial_button_3,
      $serial_ports_3
    );
  });

  // Serial 4 port events
  ipcRenderer.on('serial_open_4', (sender, data) => {
    serialOpenDisplay(
      $enable_serial_button_4,
      $disable_serial_button_4,
      $calibrate_serial_button_4,
      $serial_ports_4
    );
  });
  ipcRenderer.on('serial_closed_4', (sender, data) => {
    serialCloseDisplay(
      $enable_serial_button_4,
      $disable_serial_button_4,
      $calibrate_serial_button_4,
      $serial_ports_4
    );
  });
  ipcRenderer.on('serial_error_4', (sender, data) => {
    serialCloseDisplay(
      $enable_serial_button_4,
      $disable_serial_button_4,
      $calibrate_serial_button_4,
      $serial_ports_4
    );
  });

  // Serial BOX port events
  function serialBoxOpenDisplay() {
    $disable_serial_box_button.attr('disabled', false);
    $enable_serial_box_button.attr('disabled', true);
    $serial_box_ports.attr('disabled', true);
  }
  function serialBoxCloseDisplay() {
    $disable_serial_box_button.attr('disabled', true);
    $enable_serial_box_button.attr('disabled', false);
    $serial_box_ports.attr('disabled', false);
  }
  ipcRenderer.on('serial_box_open', (sender, data) => {
    serialBoxOpenDisplay();
  })
  ipcRenderer.on('serial_box_closed', (sender, data) => {
    serialBoxCloseDisplay();
  })
  ipcRenderer.on('serial_box_error', (sender, data) => {
    serialBoxCloseDisplay();
  })

  /*****************************************
   * SINGLE DATA RECEIVE FOR ALL ZONES
   *****************************************/
  ipcRenderer.on('serial_data', (sender, data_raw) => {
    document.getElementById("chart-serial-temperature").innerHTML = `${data_raw[1].temp_data}*F`;
    chart_utils.set_data(
      chart_force,
      chart_uniform,
      chart_master,
      data_raw[0],
      data_raw[1],
    );
  });

  // BOX - recieve data
  ipcRenderer.on('serial_box_data', (sender, [data_raw, selectedPort]) => {
    document.getElementById("chart-box-sw-delta").innerHTML = `${data_raw[1]}`;
    document.getElementById("chart-box-power-setting").innerHTML = `${data_raw[2].toFixed(2)}`;
    /*if(document.getElementById("chart-box-sw-delta").val() <= 5 && document.getElementById("chart-box-sw-delta").val() >= -5) {
      document.getElementById("chart-box-sw-delta").style.color = "green";
    } else {
      document.getElementById("chart-box-sw-delta").style.color = "red";
    }*/
    chart_utils.set_box_data(chart_master, data_raw, selectedPort)
  })

  /***********************************************
   * ZONES + BOX - BUTTON EVENTS
   ***********************************************/
  /**
   * Serial port 1 logic
   */
  // start getting serial data immediately
  $enable_serial_button.on('click', (e) => {
    let com = $('#serial_ports').val();
    if(com) {
      console.log("Enabling serial");
      chart_force.data.datasets[0] = {};
      chart_uniform.data.datasets[0] = {};
      for(let i = 0; i < 1; i++) {
        chart_master.data.datasets[0].data = [];
        chart_master.data.datasets[1].data = [];
        chart_master.data.datasets[2].data = [];
        chart_master.data.datasets[3].data = [];
        chart_master.data.datasets[4].data = [];
        chart_master.data.datasets[5].data = [];
        chart_master.data.datasets[6].data = [];
        chart_master.data.datasets[7].data = [];
        chart_master.data.datasets[8].data = [];
        chart_master.data.datasets[9].data = [];
        chart_master.data.datasets[10].data = [];
        chart_master.data.datasets[11].data = [];
        chart_master.data.datasets[12].data = [];
        chart_master.data.datasets[13].data = [];

        chart_zones.data.datasets[0].data = [];
        chart_zones.data.datasets[1].data = [];
        chart_zones.data.datasets[2].data = [];
        chart_zones.data.datasets[3].data = [];
      }
      // send null to the enable_serial channel. the main process should see this
      // later wee should send information like com port, log file, etc.
      $use_default_logname.attr('disabled', true);
      let custom_logname = $log_name.val();
      if(!$use_default_logname.prop('checked') && custom_logname) {
        // if we're using a custom logfile
        logname = custom_logname;
      }
      ipcRenderer.send('enable_serial_1', { portName: com });
    }
  });
  $disable_serial_button.on('click', (e) => {
    console.log("Disabling serial");
    // tell main process to stop receiving data.
    ipcRenderer.send('disable_serial_1');
  })
  $calibrate_serial_button.click( (e) => {
    console.log('calibrated');
    ipcRenderer.send('calibrate_serial_1');
  })

  /**
   * Serial port 2 logic
   */
  // start getting serial data immediately
  $enable_serial_button_2.on('click', (e) => {
    let com = $('#serial_ports_2').val();
    if(com) {
      console.log("Enabling serial 2");
      chart_force.data.datasets[0] = {};
      chart_uniform.data.datasets[0] = {};
      for(i = 0; i < 1; i++) {
        chart_master.data.datasets[0].data = [];
        chart_master.data.datasets[1].data = [];
        chart_master.data.datasets[2].data = [];
        chart_master.data.datasets[3].data = [];
        chart_master.data.datasets[4].data = [];
        chart_master.data.datasets[5].data = [];
        chart_master.data.datasets[6].data = [];
        chart_master.data.datasets[7].data = [];
        chart_master.data.datasets[8].data = [];
        chart_master.data.datasets[9].data = [];

        chart_zones.data.datasets[0].data = [];
        chart_zones.data.datasets[1].data = [];
        chart_zones.data.datasets[2].data = [];
        chart_zones.data.datasets[3].data = [];
      }
      // send null to the enable_serial channel. the main process should see this
      // later wee should send information like com port, log file, etc.
      $use_default_logname.attr('disabled', true);
      let custom_logname = $log_name.val();
      if(!$use_default_logname.prop('checked') && custom_logname) {
        // if we're using a custom logfile
        logname = custom_logname;
      }
      ipcRenderer.send('enable_serial_2', { portName: com });
    }
  })
  $disable_serial_button_2.on('click', (e) => {
    console.log("Disabling serial 2")
    // tell main process to stop receiving data.
    ipcRenderer.send('disable_serial_2');
  })
  $calibrate_serial_button_2.click( (e) => {
    console.log('calibrated');
    ipcRenderer.send('calibrate_serial_2');
  })

  /**
   * Serial port 3 logic
   */
  // start getting serial data immediately
  $enable_serial_button_3.on('click', (e) => {
    let com = $('#serial_ports_3').val();
    if(com) {
      console.log("Enabling serial 3");
      chart_force.data.datasets[0] = {};
      chart_uniform.data.datasets[0] = {};
      for(let i = 0; i < 1; i++) {
        chart_master.data.datasets[0].data = [];
        chart_master.data.datasets[1].data = [];
        chart_master.data.datasets[2].data = [];
        chart_master.data.datasets[3].data = [];
        chart_master.data.datasets[4].data = [];
        chart_master.data.datasets[5].data = [];
        chart_master.data.datasets[6].data = [];
        chart_master.data.datasets[7].data = [];
        chart_master.data.datasets[8].data = [];
        chart_master.data.datasets[9].data = [];
        chart_master.data.datasets[10].data = [];
        chart_master.data.datasets[11].data = [];
        chart_master.data.datasets[12].data = [];
        chart_master.data.datasets[13].data = [];
      }
      // send null to the enable_serial channel. the main process should see this
      // later wee should send information like com port, log file, etc.
      $use_default_logname.attr('disabled', true);
      let custom_logname = $log_name.val();
      if(!$use_default_logname.prop('checked') && custom_logname) {
        // if we're using a custom logfile
        logname = custom_logname;
      }
      ipcRenderer.send('enable_serial_3', { portName: com });
    }
  });
  $disable_serial_button_3.on('click', (e) => {
    console.log("Disabling serial 3");
    // tell main process to stop receiving data.
    ipcRenderer.send('disable_serial_3');
  })
  $calibrate_serial_button_3.click( (e) => {
    console.log('calibrated');
    ipcRenderer.send('calibrate_serial_3');
  })

  /**
   * Serial port 4 logic
   */
  // start getting serial data immediately
  $enable_serial_button_4.on('click', (e) => {
    let com = $('#serial_ports_4').val();
    if(com) {
      console.log("Enabling serial 4");
      chart_force.data.datasets[0] = {};
      chart_uniform.data.datasets[0] = {};
      for(i = 0; i < 1; i++) {
        chart_master.data.datasets[0].data = [];
        chart_master.data.datasets[1].data = [];
        chart_master.data.datasets[2].data = [];
        chart_master.data.datasets[3].data = [];
        chart_master.data.datasets[4].data = [];
        chart_master.data.datasets[5].data = [];
        chart_master.data.datasets[6].data = [];
        chart_master.data.datasets[7].data = [];
        chart_master.data.datasets[8].data = [];
        chart_master.data.datasets[9].data = [];
      }
      // send null to the enable_serial channel. the main process should see this
      // later wee should send information like com port, log file, etc.
      $use_default_logname.attr('disabled', true);
      let custom_logname = $log_name.val();
      if(!$use_default_logname.prop('checked') && custom_logname) {
        // if we're using a custom logfile
        logname = custom_logname;
      }
      ipcRenderer.send('enable_serial_4', { portName: com });
    }
  })
  $disable_serial_button_4.on('click', (e) => {
    // tell main process to stop receiving data.
    console.log("Disabling serial 4")
    ipcRenderer.send('disable_serial_4');
  })
  $calibrate_serial_button_4.click( (e) => {
    console.log('calibrated');
    ipcRenderer.send('calibrate_serial_4');
  })


  /**
   * Box logic
   */
  $enable_serial_box_button.on('click', (e) => {
    let com = $('#serial_box_ports').val();
    if(com) {
      console.log("Enabling box serial");
      ipcRenderer.send('enable_box_serial', { portName: com, file: ipcRenderer.sendSync('getPath', 'appData') + '/' + '_E.txt'});
    }
  })
  $disable_serial_box_button.on('click', (e) => {
    console.log("Disabling box serial");
    // tell main process to stop receiving data
    ipcRenderer.send('disable_serial_box');
  });

  /**
   * Logging logic
   */
  $enable_logging.on('click', (e) => {
    console.log("Enable logging");

    // must go first right now because it uses 'disabled' to check which ports to monitor
    startSerialErrorTracking();

    $disable_logging.attr('disabled', false);
    $enable_logging.attr('disabled', true);
    $pause_graph_button.attr('disabled', false);
    $unpause_graph_button.attr('disabled', true);

    $enable_serial_button.attr('disabled', true);
    $enable_serial_button_2.attr('disabled', true);
    $enable_serial_button_3.attr('disabled', true);
    $enable_serial_button_4.attr('disabled', true);
    $enable_serial_box_button.attr('disabled', true);

    $disable_serial_button.attr('disabled', true);
    $disable_serial_button_2.attr('disabled', true);
    $disable_serial_button_3.attr('disabled', true);
    $disable_serial_button_4.attr('disabled', true);
    $disable_serial_box_button.attr('disabled', true);

    let logname = (new Date()).toLocaleString();
    ipcRenderer.send('begin_serial_logging', {
      file1: ipcRenderer.sendSync('getPath', 'appData') + '/' + logname.replaceAll('\\W','_') + '_A.txt',
      file2: ipcRenderer.sendSync('getPath', 'appData') + '/' + logname.replaceAll('\\W','_') + '_B.txt',
      file3: ipcRenderer.sendSync('getPath', 'appData') + '/' + logname.replaceAll('\\W','_') + '_C.txt',
      file4: ipcRenderer.sendSync('getPath', 'appData') + '/' + logname.replaceAll('\\W','_') + '_D.txt',
      fileBox: ipcRenderer.sendSync('getPath', 'appData') + '/' + logname.replaceAll('\\W','_') + '_BOX.txt',
    });
    // chart_master.reset();
    chart_master.data.labels = [];
    for (let i = 0; i < 14; i++) {
      chart_master.data.datasets[i].data = [];
    }
  })

  function disableLogging() {

    endSerialErrorTracking();

    console.log("Disable logging");
    $disable_logging.attr('disabled', true);
    $enable_logging.attr('disabled', false);
    $pause_graph_button.attr('disabled', true);
    $unpause_graph_button.attr('disabled', true);
    if ($serial_box_ports.attr('disabled')) {
      $disable_serial_box_button.attr('disabled', false);
    } else {
      $enable_serial_box_button.attr('disabled', false);
    }
    if ($serial_ports.attr('disabled')) {
      $disable_serial_button.attr('disabled', false);
    } else {
      $enable_serial_button.attr('disabled', false);
    }
    if ($serial_ports_2.attr('disabled')) {
      $disable_serial_button_2.attr('disabled', false);
    } else {
      $enable_serial_button_2.attr('disabled', false);
    }
    if ($serial_ports_3.attr('disabled')) {
      $disable_serial_button_3.attr('disabled', false);
    } else {
      $enable_serial_button_3.attr('disabled', false);
    }
    if ($serial_ports_4.attr('disabled')) {
      $disable_serial_button_4.attr('disabled', false);
    } else {
      $enable_serial_button_4.attr('disabled', false);
    }
    ipcRenderer.send('end_serial_logging');
  }

  $disable_logging.on('click', (e) => {
    disableLogging();
  })

  ipcRenderer.on('log_closed', (sender, data) => {
    console.trace("in logging");
    $log_list.prepend($("<li><a href='#'>" + path.basename(data.file.path) + "</a></li>"));
  });

  // Graph axis modifiers for range sliders
  // radar charts
  let $force_scale_max = $('#force_scale_max');
  let $uniform_scale_max = $('#uniform_scale_max');

  // master chart
  let $psi_scale_min = $('#psi_scale_min');
  let $psi_scale_max = $('#psi_scale_max');
  let $flow_rate_scale_min = $('#flow_rate_scale_min');
  let $flow_rate_scale_max = $('#flow_rate_scale_max');
  let $full_current_scale_min = $('#full_current_scale_min');
  let $full_current_scale_max = $('#full_current_scale_max');
  let $full_voltage_scale_min = $('#full_voltage_scale_min');
  let $full_voltage_scale_max = $('#full_voltage_scale_max');
  let $motor_current_scale_min = $('#motor_current_scale_min');
  let $motor_current_scale_max = $('#motor_current_scale_max');
  let $motor_voltage_scale_min = $('#motor_voltage_scale_min');
  let $motor_voltage_scale_max = $('#motor_voltage_scale_max');
  let $motor_temp_scale_min = $('#motor_temp_scale_min');
  let $motor_temp_scale_max = $('#motor_temp_scale_max');
  let $encoder_position_scale_min = $('#encoder_position_scale_min');
  let $encoder_position_scale_max = $('#encoder_position_scale_max');
  let $strap_constriction_scale_min = $('#strap_constriction_scale_min');
  let $strap_constriction_scale_max = $('#strap_constriction_scale_max');
  let $estim_skin_scale_min = $('#estim_skin_scale_min');
  let $estim_skin_scale_max = $('#estim_skin_scale_max');
  let $sync_scale_min = $('#sync_scale_min');
  let $sync_scale_max = $('#sync_scale_max');
  let $motor_state_scale_min = $('#motor_state_scale_min');
  let $motor_state_scale_max = $('#motor_state_scale_max');
  let $box_current_scale_min = $('#box_current_scale_min');
  let $box_current_scale_max = $('#box_current_scale_max');
  let $box_estim_scale_min = $('#box_estim_scale_min');
  let $box_estim_scale_max = $('#box_estim_scale_max');

  // zones chart
  let $zone_1_scale_min = $('#zone_1_scale_min');
  let $zone_1_scale_max = $('#zone_1_scale_max');
  let $zone_2_scale_min = $('#zone_2_scale_min');
  let $zone_2_scale_max = $('#zone_2_scale_max');
  let $zone_3_scale_min = $('#zone_3_scale_min');
  let $zone_3_scale_max = $('#zone_3_scale_max');
  let $zone_4_scale_min = $('#zone_4_scale_min');
  let $zone_4_scale_max = $('#zone_4_scale_max');

  function update_line_range() {
    chart_force.options.scale.ticks.max = parseInt($force_scale_max.val());
    chart_uniform.options.scale.ticks.max = parseInt($uniform_scale_max.val());
  }
  $force_scale_max.on('change mousemove', update_line_range);
  $force_scale_min.on('change mousemove', update_line_range);


  const updateViewWidth = (idx, $element, chart, step=1) => {
    let prev = parseInt($element.val());
    let current = prev;

    return () => {
      current = parseInt($element.val());
      let val = (current - prev) / step;
      console.log(current, prev, val);

      chart.options.scales.yAxes[idx].ticks.min -= val;
      chart.options.scales.yAxes[idx].ticks.max += val;

      prev = current;
    }
  }

  const updateViewPosition = (idx, $element, chart, step=1) => {
    let prev = parseInt($element.val());
    let current = prev;
    return () => {
      current = parseInt($element.val());
      let val = (prev - current) / step;
      chart.options.scales.yAxes[idx].ticks.min += val;
      chart.options.scales.yAxes[idx].ticks.max += val;
      prev = current;
    }
  }

  $psi_scale_min.on('change mousemove', updateViewWidth(0, $psi_scale_min, chart_master));
  $psi_scale_max.on('change mousemove', updateViewPosition(0, $psi_scale_max, chart_master));
  $flow_rate_scale_min.on('change mousemove', updateViewWidth(1, $flow_rate_scale_min, chart_master));
  $flow_rate_scale_max.on('change mousemove', updateViewPosition(1, $flow_rate_scale_max, chart_master));
  $full_current_scale_min.on('change mousemove', updateViewWidth(2, $full_current_scale_min, chart_master));
  $full_current_scale_max.on('change mousemove', updateViewPosition(2, $full_current_scale_max, chart_master));
  $full_voltage_scale_min.on('change mousemove', updateViewWidth(3, $full_voltage_scale_min, chart_master));
  $full_voltage_scale_max.on('change mousemove', updateViewPosition(3, $full_voltage_scale_max, chart_master));
  $motor_current_scale_min.on('change mousemove', updateViewWidth(4, $motor_current_scale_min, chart_master));
  $motor_current_scale_max.on('change mousemove', updateViewPosition(4, $motor_current_scale_max, chart_master));
  $motor_voltage_scale_min.on('change mousemove', updateViewWidth(5, $motor_voltage_scale_min, chart_master));
  $motor_voltage_scale_max.on('change mousemove', updateViewPosition(5, $motor_voltage_scale_max, chart_master));
  $motor_temp_scale_min.on('change mousemove', updateViewWidth(6, $motor_temp_scale_min, chart_master));
  $motor_temp_scale_max.on('change mousemove', updateViewPosition(6, $motor_temp_scale_max, chart_master));
  $encoder_position_scale_min.on('change mousemove', updateViewWidth(7, $encoder_position_scale_min, chart_master));
  $encoder_position_scale_max.on('change mousemove', updateViewPosition(7, $encoder_position_scale_max, chart_master));
  $strap_constriction_scale_min.on('change mousemove', updateViewWidth(8, $strap_constriction_scale_min, chart_master));
  $strap_constriction_scale_max.on('change mousemove', updateViewPosition(8, $strap_constriction_scale_max, chart_master));
  $estim_skin_scale_min.on('change mousemove', updateViewWidth(9, $estim_skin_scale_min, chart_master));
  $estim_skin_scale_max.on('change mousemove', updateViewPosition(9, $estim_skin_scale_max, chart_master));
  $sync_scale_min.on('change mousemove', updateViewWidth(10, $sync_scale_min, chart_master));
  $sync_scale_max.on('change mousemove', updateViewPosition(10, $sync_scale_max, chart_master));
  $motor_state_scale_min.on('change mousemove', updateViewWidth(11, $motor_state_scale_min, chart_master));
  $motor_state_scale_max.on('change mousemove', updateViewPosition(11, $motor_state_scale_max, chart_master));
  $box_current_scale_min.on('change mousemove', updateViewWidth(12, $box_current_scale_min, chart_master));
  $box_current_scale_max.on('change mousemove', updateViewPosition(12, $box_current_scale_max, chart_master));
  $box_estim_scale_min.on('change mousemove', updateViewWidth(13, $box_estim_scale_min, chart_master));
  $box_estim_scale_max.on('change mousemove', updateViewPosition(13, $box_estim_scale_max, chart_master));

  $zone_1_scale_min.on('change mousemove', updateViewWidth(0, $zone_1_scale_min, chart_zones));
  $zone_1_scale_max.on('change mousemove', updateViewPosition(0, $zone_1_scale_max, chart_zones));
  $zone_2_scale_min.on('change mousemove', updateViewWidth(1, $zone_2_scale_min, chart_zones));
  $zone_2_scale_max.on('change mousemove', updateViewPosition(1, $zone_2_scale_max, chart_zones));
  $zone_3_scale_min.on('change mousemove', updateViewWidth(2, $zone_3_scale_min, chart_zones));
  $zone_3_scale_max.on('change mousemove', updateViewPosition(2, $zone_3_scale_max, chart_zones));
  $zone_4_scale_min.on('change mousemove', updateViewWidth(3, $zone_4_scale_min, chart_zones));
  $zone_4_scale_max.on('change mousemove', updateViewPosition(3, $zone_4_scale_max, chart_zones));

  // Master chart checkbox logic
  let $y_axes_checkbox = $('#y_axes_checkbox');
  let $psi_checkbox = $('#psi_checkbox');
  let $flow_rate_checkbox = $('#flow_rate_checkbox');
  let $full_current_checkbox = $('#full_current_checkbox');
  let $full_voltage_checkbox = $('#full_voltage_checkbox');
  let $motor_current_checkbox = $('#motor_current_checkbox');
  let $motor_voltage_checkbox = $('#motor_voltage_checkbox');
  let $motor_temp_checkbox = $('#motor_temp_checkbox');
  let $encoder_position_checkbox = $('#encoder_position_checkbox');
  let $strap_constriction_checkbox = $('#strap_constriction_checkbox');
  let $estim_skin_checkbox = $('#estim_skin_checkbox');
  let $sync_checkbox = $('#sync_checkbox');
  let $motor_state_checkbox = $('#motor_state_checkbox');
  let $box_current_checkbox = $('#box_current_checkbox');
  let $box_estim_checkbox = $('#box_estim_checkbox');

  $y_axes_checkbox.on('click', renderMultiChart);
  $psi_checkbox.on('click', renderMultiChart);
  $flow_rate_checkbox.on('click', renderMultiChart); $full_current_checkbox.on('click', renderMultiChart);
  $full_voltage_checkbox.on('click', renderMultiChart);
  $motor_current_checkbox.on('click', renderMultiChart);
  $motor_voltage_checkbox.on('click', renderMultiChart);
  $motor_temp_checkbox.on('click', renderMultiChart);
  $encoder_position_checkbox.on('click', renderMultiChart);
  $strap_constriction_checkbox.on('click', renderMultiChart);
  $estim_skin_checkbox.on('click', renderMultiChart);
  $sync_checkbox.on('click', renderMultiChart);
  $motor_state_checkbox.on('click', renderMultiChart);
  $box_current_checkbox.on('click', renderMultiChart);
  $box_estim_checkbox.on('click', renderMultiChart);

  function renderMultiChart() {
    if(document.getElementById('y_axes_checkbox').checked == true) {
      for(let i = 0; i < 14; i++) {
       chart_master.options.scales.yAxes[i].display = true;
       document.getElementById('master_chart_slider').style.display = 'block';
      }
    } else {
      for(let i = 0; i < 14; i++) {
        chart_master.options.scales.yAxes[i].display = false;
        document.getElementById('master_chart_slider').style.display = 'none';
      }
    }
    if(document.getElementById('psi_checkbox').checked == true) {
      chart_master.data.datasets[0].borderColor = '#FF8A80';
    } else {
      chart_master.data.datasets[0].borderColor = 'rgba(0,0,0,0)';
    }
    if(document.getElementById('flow_rate_checkbox').checked == true) {
      chart_master.data.datasets[1].borderColor = '#FF80AB';
    } else {
      chart_master.data.datasets[1].borderColor = 'rgba(0,0,0,0)';
    }
    if(document.getElementById('full_current_checkbox').checked == true) {
      chart_master.data.datasets[2].borderColor = '#AED581';
    } else {
      chart_master.data.datasets[2].borderColor = 'rgba(0,0,0,0)';
    }
    if(document.getElementById('full_voltage_checkbox').checked == true) {
      chart_master.data.datasets[3].borderColor = '#48be65';
    } else {
      chart_master.data.datasets[3].borderColor = 'rgba(0,0,0,0)';
    }
    if(document.getElementById('motor_current_checkbox').checked == true) {
      chart_master.data.datasets[4].borderColor = '#EA80FC';
    } else {
      chart_master.data.datasets[4].borderColor = 'rgba(0,0,0,0)';
    }
    if(document.getElementById('motor_voltage_checkbox').checked == true) {
      chart_master.data.datasets[5].borderColor = '#B388FF';
    } else {
      chart_master.data.datasets[5].borderColor = 'rgba(0,0,0,0)';
    }
    if(document.getElementById('motor_temp_checkbox').checked == true) {
      chart_master.data.datasets[6].borderColor = '#8C9EFF';
    } else {
      chart_master.data.datasets[6].borderColor = 'rgba(0,0,0,0)';
    }
    if(document.getElementById('encoder_position_checkbox').checked == true) {
      chart_master.data.datasets[7].borderColor = '#80D8FF';
    } else {
      chart_master.data.datasets[7].borderColor = 'rgba(0,0,0,0)';
    }
    if(document.getElementById('strap_constriction_checkbox').checked == true) {
      chart_master.data.datasets[8].borderColor = '#4DB6AC';
    } else {
      chart_master.data.datasets[8].borderColor = 'rgba(0,0,0,0)';
    }
    if(document.getElementById('estim_skin_checkbox').checked == true) {
      chart_master.data.datasets[9].borderColor = '#fa2020';
    } else {
      chart_master.data.datasets[9].borderColor = 'rgba(0,0,0,0)';
    }
    if(document.getElementById('sync_checkbox').checked == true) {
      chart_master.data.datasets[10].borderColor = '#FFB74D';
    } else {
      chart_master.data.datasets[10].borderColor = 'rgba(0,0,0,0)';
    }
    if(document.getElementById('motor_state_checkbox').checked == true) {
      chart_master.data.datasets[11].borderColor = 'rgb(108, 158, 51)';
    } else {
      chart_master.data.datasets[11].borderColor = 'rgba(0,0,0,0)';
    }
    if(document.getElementById('box_current_checkbox').checked == true) {
      chart_master.data.datasets[12].borderColor = 'rgb(149, 179, 27)';
    } else {
      chart_master.data.datasets[12].borderColor = 'rgba(0,0,0,0)';
    }
    if(document.getElementById('box_estim_checkbox').checked == true) {
      chart_master.data.datasets[13].borderColor = 'rgb(182, 179, 36)';
    } else {
      chart_master.data.datasets[13].borderColor = 'rgba(0,0,0,0)';
    }
  }

  let zone_1_tab = $('#zone-1-tab');
  let zone_2_tab = $('#zone-2-tab');
  let zone_3_tab = $('#zone-3-tab');
  let zone_4_tab = $('#zone-4-tab');
  let zone_tabs = $('#zone-tabs');

  zone_tabs.on('click', (e) => {
    if (zone_1_tab.attr('aria-expanded') == 'true') {
      ipcRenderer.send('change_port', PORTS.PORT_1);
    }
    if (zone_2_tab.attr('aria-expanded') == 'true') {
      ipcRenderer.send('change_port', PORTS.PORT_2);
    }
    if (zone_3_tab.attr('aria-expanded') == 'true') {
      ipcRenderer.send('change_port', PORTS.PORT_3);
      if ($serial_ports_3.attr('disabled')) {
        chart_master.data.labels = [];
        chart_utils.clear_data(master_chart, 6);
      }
    }
    if (zone_4_tab.attr('aria-expanded') == 'true') {
      ipcRenderer.send('change_port', PORTS.PORT_4);
    }
  })

  let $zones_chart_point = $('#zones_chart_point');
  //let zones_min = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
  //let zones_max = [6, 1000, 1000, 500, 200, 500, 500, 500, 500, 50, 50, 50, 50, 50];
  //$zones_chart_point.on('change', zones_chart_change)


  //function zones_chart_change () {
      //$zone_1_scale_min.attr('min', zones_min[$zones_chart_point.val()]);
      //$zone_1_scale_max.attr('max', zones_max[$zones_chart_point.val()]);
      //$zone_2_scale_min.attr('min', zones_min[$zones_chart_point.val()]);
      //$zone_2_scale_max.attr('max', zones_max[$zones_chart_point.val()]);
      //$zone_3_scale_min.attr('min', zones_min[$zones_chart_point.val()]);
      //$zone_3_scale_max.attr('max', zones_max[$zones_chart_point.val()]);
      //$zone_4_scale_min.attr('min', zones_min[$zones_chart_point.val()]);
      //$zone_4_scale_max.attr('max', zones_max[$zones_chart_point.val()]);
  //}

  let $zones_chart_yaxes = $('#zones_chart_yaxes');
  $zones_chart_yaxes.on('click', zones_chart_yaxes);

  let toggleBtn = false;
  function zones_chart_yaxes () {
    if(!toggleBtn) {
      toggleBtn = true;
      for(let i = 0; i < 4; i++) {
        chart_zones.options.scales.yAxes[i].display = true;
      }
      document.getElementById('zones_chart_slider').style.display = 'block';
    } else {
      toggleBtn = false;
      for(let i = 0; i < 4; i++) {
        chart_zones.options.scales.yAxes[i].display = false;
      }
      document.getElementById('zones_chart_slider').style.display = 'none';
    }
  }

  /**
   * Chart pause buttons
   */

  $pause_graph_button.on('click', (e) => {
    if (chartUpdater == null) {
      return;
    }

    chart_utils.stop_draw_cycle(chartUpdater);
    chartUpdater = null
    $pause_graph_button.attr('disabled', true);
    $unpause_graph_button.attr('disabled', false);
  })
  $('#unpause_graph_button').on('click', (e) => {
    if (chartUpdater != null) {
      return;
    }
    chartUpdater = chart_utils.start_draw_cycle([
    chart_force,
    chart_uniform,
    chart_master,
    chart_zones
  ]);
  $pause_graph_button.attr('disabled', false);
  $unpause_graph_button.attr('disabled', true);
  })
});
