// hsl to rgb functions. For some reason, chartkick doesn't like hsl.
// technically vendor functions, from https://gist.github.com/mjackson/5311256
function hue2rgb(p, q, t) { if (t < 0) t += 1; if (t > 1) t -= 1; if (t < 1/6) return p + (q - p) * 6 * t; if (t < 1/2) return q; if (t < 2/3) return p + (q - p) * (2/3 - t) * 6; return p; }
function hslToRgb(h, s, l) { var r, g, b; if (s == 0) { r = g = b = l; } else { var q = l < 0.5 ? l * (1 + s) : l + s - l * s; var p = 2 * l - q; r = hue2rgb(p, q, h + 1/3); g = hue2rgb(p, q, h); b = hue2rgb(p, q, h - 1/3); } return [ r * 255, g * 255, b * 255 ]; }

// update two charts with data.
function set_data(
  force_chart,
  uniform_chart,
  master_chart,
  data,
  processed,
  line_limit = 512,
  forward = true
) {
  // Update radar chart with color, data, and labels.
  let force_color = hslToRgb(processed.force_data*.25, 1, .5);
  let uni_color = hslToRgb(processed.force_data*.25, 1, .5);
  let radar_scale_raw =  $('#force_scale_max')[0].value;
  let radar_scale_uniform = $('#uniform_scale_max')[0].value;
  // This data is coming in raw (which is why it is 1-indexed)
  force_chart.data.datasets[0].backgroundColor = "rgba("+force_color[0]+","+force_color[1]+","+force_color[2]+",.7)";
  force_chart.data.datasets[0].data = [radar_scale_raw-data[1], radar_scale_raw-data[2], radar_scale_raw-data[3], radar_scale_raw-data[4], radar_scale_raw-data[5], radar_scale_raw-data[6], radar_scale_raw-data[7], radar_scale_raw-data[8]];
  force_chart.data.labels = [`1: [${data[1]}]`, `2: [${data[2]}]`, `3: [${data[3]}]`, `4: [${data[4]}]`, `5: [${data[5]}]`, `6: [${data[6]}]`, `7: [${data[7]}]`, `8: [${data[8]}]`];

  // This data is coming from a prcesosor (which is why it is 0-indexed)
  uniform_chart.data.datasets[0].backgroundColor = "rgba("+uni_color[0]+","+uni_color[1]+","+uni_color[2]+",.7)";
  uniform_chart.data.datasets[0].data = [radar_scale_uniform-processed.uniform_data[0], radar_scale_uniform-processed.uniform_data[1], radar_scale_uniform-processed.uniform_data[2], radar_scale_uniform-processed.uniform_data[3], radar_scale_uniform-processed.uniform_data[4], radar_scale_uniform-processed.uniform_data[5], radar_scale_uniform-processed.uniform_data[6], radar_scale_uniform-processed.uniform_data[7]];
  uniform_chart.data.labels = [`1: [${processed.uniform_data[0].toFixed(2)}]`, `2: [${processed.uniform_data[1].toFixed(2)}]`, `3: [${processed.uniform_data[2].toFixed(2)}]`, `4: [${processed.uniform_data[3].toFixed(2)}]`, `5: [${processed.uniform_data[4].toFixed(2)}]`, `6: [${processed.uniform_data[5].toFixed(2)}]`, `7: [${processed.uniform_data[6].toFixed(2)}]`, `8: [${processed.uniform_data[7].toFixed(2)}]`];

  if (forward) {
    // add a time label to the x axis of the pressure chart
    master_chart.data.labels.push(data[0]);
    // loop through each pressure sensor
    // add the new data to the chart
    // master_chart.data.datasets[0].data.push({ t: data[0], y: processed.pressure_data }); // FSR Sensors
    master_chart.data.datasets[0].data.push({ t: data[0], y: processed.psi_data }); // PSI
    master_chart.data.datasets[1].data.push({ t: data[0], y: data[10] }); // Flow Rate
    master_chart.data.datasets[4].data.push({ t: data[0], y: data[11] }); // Motor Current
    master_chart.data.datasets[5].data.push({ t: data[0], y: data[12] }); // Motor Voltage
    master_chart.data.datasets[6].data.push({ t: data[0], y: processed.temp_data }); // Motor Temp
    master_chart.data.datasets[7].data.push({ t: data[0], y: data[14] }); // Barrel Position
    master_chart.data.datasets[8].data.push({ t: data[0], y: processed.encoder_max }); // Strap Constriction
    master_chart.data.datasets[9].data.push({ t: data[0], y: data[16] }); // Estim at Skin
    // master_chart.data.datasets[10].data.push({ t: data[0], y: data[17] }); // Compression and Estim

    // if we have more than 'line_limit' data points in the chart, shift out the first data point and label.
    if (master_chart.data.labels.length > line_limit) {
      master_chart.data.labels.shift()
      // master_chart.data.datasets[0].data.shift(); // FSR Sensors
      master_chart.data.datasets[0].data.shift() // PSI
      master_chart.data.datasets[1].data.shift() // Flow Rate
      master_chart.data.datasets[4].data.shift() // Motor Current
      master_chart.data.datasets[5].data.shift() // Motor Voltage
      master_chart.data.datasets[6].data.shift() // Motor Temp
      master_chart.data.datasets[7].data.shift() // Barrel Position
      master_chart.data.datasets[8].data.shift() // Strap Constriction
      master_chart.data.datasets[9].data.shift() // Estim at Skin
      // master_chart.data.datasets[10].data.shift() // Compression and Estim
      if (master_chart.data.datasets[11].data != '' &&
            master_chart.data.datasets[11].data[0].t < master_chart.data.datasets[0].data[0].t) {
        master_chart.data.datasets[11].data.shift(); // Motor State
        master_chart.data.datasets[12].data.shift(); // Current Draw of Motor
        // master_chart.data.datasets[13].data.shift(); // Estim Desired
      }
    }
  } else {
    master_chart.data.labels.unshift(data[0]);
    master_chart.data.datasets[0].data.unshift({t: data[0], y: processed.psi_data }); // PSI
    master_chart.data.datasets[1].data.unshift({ t: data[0], y: data[10] }); // Flow Rate
    master_chart.data.datasets[4].data.unshift({ t: data[0], y: data[11] }); // Motor Current
    master_chart.data.datasets[5].data.unshift({ t: data[0], y: data[12] }); // Motor Voltage
    master_chart.data.datasets[6].data.unshift({ t: data[0], y: processed.temp_data }); // Motor Temp
    master_chart.data.datasets[7].data.unshift({ t: data[0], y: data[14] }); // Barrel Position
    master_chart.data.datasets[8].data.unshift({ t: data[0], y: processed.encoder_max }); // Strap Constriction
    master_chart.data.datasets[9].data.unshift({ t: data[0], y: data[16] }); // Estim at Skin
    // master_chart.data.datasets[10].data.unshift({ t: data[0], y: data[17] }); // Compression and Estim
    master_chart.data.datasets[11].data.unshift(); // Motor State
    // master_chart.data.datasets[12].data.unshift(); // Current Draw of Motor
    // master_chart.data.datasets[13].data.unshift(); // Estim Desired

    // Assume we have hit the data limit
    master_chart.data.labels.pop()
    // master_chart.data.datasets[0].data.pop(); // FSR Sensors
    master_chart.data.datasets[0].data.pop() // PSI
    master_chart.data.datasets[1].data.pop() // Flow Rate
    master_chart.data.datasets[4].data.pop() // Motor Current
    master_chart.data.datasets[5].data.pop() // Motor Voltage
    master_chart.data.datasets[6].data.pop() // Motor Temp
    master_chart.data.datasets[7].data.pop() // Barrel Position
    master_chart.data.datasets[8].data.pop() // Strap Constriction
    master_chart.data.datasets[9].data.pop() // Estim at Skin
    // master_chart.data.datasets[10].data.pop() // Compression and Estim
    master_chart.data.datasets[11].data.pop(); // Motor State
    // master_chart.data.datasets[12].data.pop(); // Current Draw of Motor
    // master_chart.data.datasets[13].data.pop(); // Estim Desired
  }
}

function set_zones_data(zone, zones_chart, data, processed, dp = 0, line_limit = 512) {
  if (dp > 10) return; // return if there displaying box data

  let point_list = [processed.psi_data, data[10], data[11], data[12], processed.temp_data, data[14], data[15], data[16], data[17], data[3], data[4 + data[3]], null, null, null]

  zones_chart.data.datasets[zone].data.push(point_list[dp]); // Average PSI

  if(zones_chart.data.datasets[zone].data.length > line_limit) {
    zones_chart.data.datasets[zone].data.shift(); // Average PSI
  }
}

function set_box_zones(zones_chart, data, dp = 11, line_limit = 512) {
  if (dp < 11) return; // in the wrong zone

  // TODO: idk if this works; if 12 is selected w/ 8 motors it will be 8
  let box_type = (dp - 11) * data[3]; // first set is state, second is current, third is estim

  console.log(data);

  for (let zone_index = 0; zone_index < data[3]; zone_index++) {
    console.log(zone_index, data[4+zone_index+box_type]);

    zones_chart.data.datasets[zone_index].data.push(data[4+zone_index+box_type]);

    if (zones_chart.data.datasets[zone_index].data.length > line_limit) {
      zones_chart.data.datasets[zone_index].data.shift();
    }

    if (zone_index == 3) break; // 4 points not yet implemented
  }

  //if (zones_chart.data.datasets[0].data.length > line_limit) {
    //zones_chart.data.datasets[0].data.shift();
    //zones_chart.data.datasets[1].data.shift();
    //zones_chart.data.datasets[2].data.shift();
    //zones_chart.data.datasets[3].data.shift();
  //}
}

function set_box_data(master_chart, data, selected_port, line_limit = 512) { // Commented out until we get box working
  let time = 0;
  if (master_chart.data.labels.length > 0) {
    time = master_chart.data.labels[master_chart.data.labels.length - 1];
  } else {
    time = data[0];
  }
  //  master_chart.data.datasets[2].data.push({ t: data[0], y: data[3]}); // Full Current
  //  master_chart.data.datasets[3].data.push({ t: data[0], y: data[4 + data[3]]}); // Full Voltage
   master_chart.data.datasets[11].data.push({ t: time, y: data[3 + selected_port]}); // Motor State
   master_chart.data.datasets[12].data.push({ t: time, y: data[3 + selected_port + data[3]]}); // Current Draw of Motor
  //  master_chart.data.datasets[13].data.push({ t: data[0], y: data[3 + selected_port + data[3]]}); // Estim Desired*/


  // if we have more than 'line_limit' data points in the chart, shift out the first data point and label.
  if ( master_chart.data.labels.length == 0 && master_chart.data.datasets[11].data.length > line_limit) {
    master_chart.data.labels.shift();
    // master_chart.data.datasets[2].data.shift(); // Full Current
    // master_chart.data.datasets[3].data.shift(); // Full Voltage
    master_chart.data.datasets[11].data.shift(); // Motor State
    master_chart.data.datasets[12].data.shift(); // Current Draw of Motor
    // master_chart.data.datasets[13].data.shift(); // Estim Desired
  }
  // console.log(master_chart.data);
}

function clear_data(chart, type) {
  if (type == 6) {
    chart.data.datasets[0] = [];
    chart.data.datasets[1] = [];
    chart.data.datasets[2] = [];
    chart.data.datasets[3] = [];
    chart.data.datasets[4] = [];
    chart.data.datasets[5] = [];
    chart.data.datasets[6] = [];
    chart.data.datasets[7] = [];
    chart.data.datasets[8] = [];
    chart.data.datasets[9] = [];
    chart.data.datasets[10] = [];
    chart.data.datasets[11] = [];
    chart.data.datasets[12] = [];
  }
}

function start_draw_cycle(charts) {
  return setInterval(()=>{
    for(i in charts)
      charts[i].update()
  },17)
}
function stop_draw_cycle(id) {
  clearInterval(id);
}
function get_ctx(id) { return  document.getElementById(id).getContext('2d'); }
function make_chart(id, type, datasets = [{}], yAxes = [], radar_scale_raw = 450, radar_scale_uniform = 100) {
  if (0 == type) {
    return new Chart(get_ctx(id), {
      type: 'radar',
      data: {
        labels: ['1', '2', '3', '4', '5', '6', '7', '8'],
        datasets: []
      },
      options: {
        animation: { duration: 0 },
        responsive: true,
        legend: { display: false },
        scale: {
          ticks: {
            display: false,
            maxTicksLimit: 5,
            min: 0,
            max: radar_scale_raw
          }
        }
      }
    });
  }
  if (1 == type) {
    return new Chart(get_ctx(id), {
      type: 'radar',
      data: {
        labels: ['1', '2', '3', '4', '5', '6', '7', '8'],
        datasets: []
      },
      options: {
        animation: { duration:0 },
        responsive: true,
        legend: { display: false },
        scale: {
          ticks: {
            display: false,
            maxTicksLimit: 5,
            min: 0,
            max: radar_scale_uniform
         }
        }
      }
    });
  }
  if (2 == type) {
    return new Chart(get_ctx(id), {
      type: 'line',
      data: {
        datasets: datasets
      },
      options: {
        animation: { duration: 0 },
        responsive: true,
        legend: { display: false },
				scales: {
					xAxes: [{
						type: 'time',
						time: { unit: 'millisecond', distribution: 'linear', tooltipFormat: 'X.SSS', displayFormats: { millisecond: 'X.SSS' } },
						scaleLabel: { display: false, labelString: 'Time' }
					}],
					yAxes: yAxes
				}
      }
    });
  }
  if (4 == type) {
    return new Chart(get_ctx(id), {
      type: 'line',
      data: {
        labels: new Array(512).fill(""),
        datasets: [
          { label:  '1',  borderColor: '#FF0000', backgroundColor: 'rgba(0,0,0,0)', yAxisID: 'yA', fill:false }, // psi
          { label:  '2',  borderColor: '#FF9900', backgroundColor: 'rgba(0,0,0,0)', yAxisID: 'yB', fill:false },
          { label:  '3',  borderColor: '#0000FF', backgroundColor: 'rgba(0,0,0,0)', yAxisID: 'yC', fill:false },
          { label:  '4',  borderColor: '#008000', backgroundColor: 'rgba(0,0,0,0)', yAxisID: 'yD', fill:false }
        ]
      },
      options: {
        animation: { duration: 0 },
        responsive: true,
        events: [],
        legend: { display: false },
				scales: {
          xAxes: [{
            gridLines: {display: false},
          }],
					yAxes: [
            { id: 'yA', display: false, ticks: {fontColor: '#FF0000', min: 0, max: 10} }, // psi
            { id: 'yB', display: false, ticks: {fontColor: '#FF9900', min: 0, max: 10} },
            { id: 'yC', display: false, ticks: {fontColor: '#0000FF', min: 0, max: 10} },
            { id: 'yD', display: false, ticks: {fontColor: '#008000', min: 0, max: 10} }
          ]
				}
      }
    });
  }
  if (6 == type) {
    return new Chart(get_ctx(id), {
      type: 'line',
      data: {
        datasets: [
          { label:  '1',  borderColor: '#FF8A80', backgroundColor: 'rgba(0,0,0,0)', yAxisID: 'yA', fill:false }, // psi
          { label:  '2',  borderColor: '#FF80AB', backgroundColor: 'rgba(0,0,0,0)', yAxisID: 'yB', fill:false },
          { label:  '3',  borderColor: '#EA80FC', backgroundColor: 'rgba(0,0,0,0)', yAxisID: 'yC', fill:false },
          { label:  '4',  borderColor: '#B388FF', backgroundColor: 'rgba(0,0,0,0)', yAxisID: 'yD', fill:false },
          { label:  '5',  borderColor: '#EA80FC', backgroundColor: 'rgba(0,0,0,0)', yAxisID: 'yE', fill:false },
          { label:  '6',  borderColor: '#B388FF', backgroundColor: 'rgba(0,0,0,0)', yAxisID: 'yF', fill:false },
          { label:  '7',  borderColor: '#8C9EFF', backgroundColor: 'rgba(0,0,0,0)', yAxisID: 'yG', fill:false }, // motor temp
          { label:  '8',  borderColor: '#80D8FF', backgroundColor: 'rgba(0,0,0,0)', yAxisID: 'yH', fill:false },
          { label:  '9',  borderColor: '#4DB6AC', backgroundColor: 'rgba(0,0,0,0)', yAxisID: 'yI', fill:false },
          { label: '10',  borderColor: '#fa2020', backgroundColor: 'rgba(0,0,0,0)', yAxisID: 'yJ', fill:false },
          { label: '11',  borderColor: '#AED581', backgroundColor: 'rgba(0,0,0,0)', yAxisID: 'yK', fill:false },
          { label: '12',  borderColor: '#6c9e33', backgroundColor: 'rgba(0,0,0,0)', yAxisID: 'yL', fill:false },
          { label: '13',  borderColor: '#95aa1b', backgroundColor: 'rgba(0,0,0,0)', yAxisID: 'yM', fill:false },
          { label: '14',  borderColor: '#b6b324', backgroundColor: 'rgba(0,0,0,0)', yAxisID: 'yN', fill:false }
        ]
      },
      options: {
        animation: { duration: 0 },
        responsive: true,
        legend: { display: false },
        events: [],
				scales: {
					xAxes: [{
						type: 'time',
						time: { unit: 'millisecond', distribution: 'linear', tooltipFormat: 'X.SSS', displayFormats: { millisecond: 'X.SSS' } },
						scaleLabel: { display: false, labelString: 'Time' }
					}],
					yAxes: [
            { id: 'yA', display: false, ticks: {fontColor: '#FF8A80', min: 0, max: 10} }, // psi
            { id: 'yB', display: false, ticks: {fontColor: '#FF80AB', min: 0, max: 10} },
            { id: 'yC', display: false, ticks: {fontColor: '#EA80FC', min: 0, max: 10} },
            { id: 'yD', display: false, ticks: {fontColor: '#B388FF', min: 250, max: 700} },
            { id: 'yE', display: false, ticks: {fontColor: '#EA80FC', min: 400, max: 1000} },
            { id: 'yF', display: false, ticks: {fontColor: '#B388FF', min: 400, max: 1000} },
            { id: 'yG', display: false, ticks: {fontColor: '#8C9EFF', min: 70, max: 130} }, // motor temp
            { id: 'yH', display: false, ticks: {fontColor: '#80D8FF', min: 0, max: 700} },
            { id: 'yI', display: false, ticks: {fontColor: '#4DB6AC', min: 0, max: 10} },
            { id: 'yJ', display: false, ticks: {fontColor: '#fa2020', min: 0, max: 500} },
            { id: 'yK', display: false, ticks: {fontColor: '#AED581', min: 0, max: 10} },
            { id: 'yL', display: false, ticks: {fontColor: '#6c9e33', min: 0, max: 10} },
            { id: 'yM', display: false, ticks: {fontColor: '#95aa1b', min: 0, max: 10} },
            { id: 'yN', display: false, ticks: {fontColor: '#b6b324', min: 0, max: 10} }
          ]
				}
      }
    });
  }
  return new Chart(get_ctx(id), {
      type: 'line',
      data: {
          datasets: [
            { lineTension: 0, borderColor: 'rgba(255,50,50)', fill:false },
            // { label: "Pressure Sensor 2", lineTension: 0, borderColor: 'rgba(255,155,50)', fill:false },
          ]
      },
      options: {
        animation: { duration: 0 },
        responsive: true,
        legend: { display: false },
				scales: {
					xAxes: [{
						type: 'time',
						time: { unit: 'millisecond', distribution: 'linear', tooltipFormat: 'X.SSS', displayFormats: { millisecond: 'X.SSS' } },
						scaleLabel: { display: true, labelString: 'Time' }
					}],
					yAxes: [
            {
              ticks: {min: 0, max: 10}
            },
          ]
				}
      }
  });
}

module.exports = {
  set_data,
  set_zones_data,
  set_box_data,
  set_box_zones,
  start_draw_cycle,
  stop_draw_cycle,
  make_chart,
  clear_data
}
