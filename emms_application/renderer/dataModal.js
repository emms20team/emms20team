const {analyzeData} = require('../main/dataFile.js')
var data = {};

const getZoneLetter = zonePath => zonePath.slice(-5, -4);

function receiveData(d, dKeys, selectedKey = 0) {
  data = d[dKeys[selectedKey]];
  $.get("./dataModal.html", m => {

    $("body").append(m);

    let $dataContainer = $(".data-analysis-container");

    let $dataDropDown1 = $(".data-drop-down1");
    let $dataDropDown2 = $(".data-drop-down2");
    let $dataAnalysis = $(".data-analysis");
    let $dataCompareBtn = $(".data-compare-btn");
    let $dataHideBtn = $(".data-hide-btn");
    let $compAnalysis = $(".comp-analysis");

    let $zoneDropDown1 = $(".zone-drop-down1");
    let $zoneDropDown2 = $(".zone-drop-down2");
    let $zoneDropDownValue = $(".zone-drop-down-value");
    let $zoneAnalysis = $(".zone-analysis");
    let $zoneCompareBtn = $(".zone-compare-btn");
    let $zoneHideBtn = $(".zone-hide-btn");
    let $zoneCompAnalysis = $(".zone-comp-analysis");

    let $dataZoneTabs = $("#data-zone-tabs");

    // handle zone tab button press
    const zoneTabClick = e => {
      data = d[e.target.id];
      populateDataModalFields(data);
    }

    // add zone tab buttons
    dKeys.forEach((key, idx) => {
      let $zoneTab = $(
        `<li><a href=# id="${key}">Zone ${key.slice(-5,-4)}</a></li>`
      );
      $zoneTab.click(zoneTabClick);
      $dataZoneTabs.append($zoneTab);
      $zoneDropDown1.append(`<option value=${idx}>Zone ${key.slice(-5, -4)}</option>`)
      $zoneDropDown2.append(`<option value=${idx}>Zone ${key.slice(-5, -4)}</option>`)
    });

    // populate zone value drop down options
    Object.keys(d[dKeys[selectedKey]]).forEach((k, i) => {
      if (k !== "time") {
        $zoneDropDownValue.append(`<option value=${i}>${k}</option>`)
      }
    });

    // TODO: why am i dynamically appending this button?
    $(".data-modal-header").append($('<button style="float:right">X</button>')
      .click(function () {
        $(".data-modal-main").remove();
      }));

    $dataHideBtn.click(() => $compAnalysis.empty());

    $dataCompareBtn.click(() => {
      $compAnalysis.empty();
      let val1 = $dataDropDown1.children("option").filter(":selected").text();
      let val2 = $dataDropDown2.children("option").filter(":selected").text();
      let compData = analyzeData(data[val1], data[val2]);
      let {avgStartDiff, avgEndDiff, avgPeakDiff, avgPeakValueDiff} = compData;

      $compAnalysis.prepend(`
        <div class="data-comp" style="display:flex;flex-direction:row;">
          <div class="data-comp-time" style="padding:30px;">
            <h5>Time Differences</h5>
            <p>
              ${val1} cycles start ${Math.abs(avgStartDiff.toFixed(2))} ms
              ${avgStartDiff >= 0 ? "after": "before"}
              ${val2}
            </p>
            <p>
              ${val1} cycles end ${Math.abs(avgEndDiff.toFixed(2))} ms
              ${avgEndDiff >= 0 ? "after": "before"}
              ${val2}
            </p>
            <p>
              ${val1} cycles peak ${Math.abs(avgPeakDiff.toFixed(2))} ms
              ${avgPeakDiff >= 0 ? "after": "before"}
              ${val2}
            </p>
          </div>
          <div class="data-comp-values" style="padding:30px">
            <h5>Value Differences</h5>
            <p>
              ${val1} peaks are ${avgPeakValueDiff >= 0 ? "higher": "lower"}
              than ${val2} peaks by ${Math.abs(avgPeakValueDiff.toFixed(2))}
            </p>
          </div>
        </div>
      `);
    });

    $zoneHideBtn.click(() => $zoneCompAnalysis.empty());

    $zoneCompareBtn.click(() => {
      $zoneCompAnalysis.empty();
      let zone1 = dKeys[$zoneDropDown1.children("option").filter(":selected").val()];
      let zone2 = dKeys[$zoneDropDown2.children("option").filter(":selected").val()];
      let value = $zoneDropDownValue.children("option").filter(":selected").text();

      let compData = analyzeData(d[zone1][value], d[zone2][value]);
      let {avgStartDiff, avgEndDiff, avgPeakDiff, avgPeakValueDiff} = compData;

      let zone1Letter = getZoneLetter(zone1);
      let zone2Letter = getZoneLetter(zone2);

      $compAnalysis.prepend(`
        <div class="zone-comp" style="display:flex;flex-direction:row;">
          <div class="zone-comp-time" style="padding:30px;">
            <h5>Time Differences</h5>
            <p>
              Zone ${zone1Letter}s ${value} cycles start ${Math.abs(avgStartDiff.toFixed(2))} ms
              ${avgStartDiff >= 0 ? "after": "before"}
              Zone ${zone2Letter}s ${value}
            </p>
            <p>
              Zone ${zone1Letter}s ${value} cycles end ${Math.abs(avgEndDiff.toFixed(2))} ms
              ${avgEndDiff >= 0 ? "after": "before"}
              Zone ${zone2Letter}s ${value}
            </p>
            <p>
              Zone ${zone1Letter}s ${value} cycles peak ${Math.abs(avgPeakDiff.toFixed(2))} ms
              ${avgPeakDiff >= 0 ? "after": "before"}
              Zone ${zone2Letter}s ${value}
            </p>
          </div>
          <div class="zone-comp-values" style="padding:30px">
            <h5>Value Differences</h5>
            <p>
              Zone ${zone1Letter}s ${value} peaks are ${avgPeakValueDiff >= 0 ? "higher": "lower"}
              than Zone ${zone2Letter}s ${value} peaks by ${Math.abs(avgPeakValueDiff.toFixed(2))}
            </p>
          </div>
        </div>
      `);
    });



    let {time} = data;

    $dataContainer.prepend(`
      <div class="data-item">
        <h3>Time: ${time} ms</h3>
      </div>`
    );

    const populateDataModalFields = data => {
      $dataDropDown1.empty();
      $dataDropDown2.empty();
      $dataAnalysis.empty();

      Object.keys(data).forEach(k => {
        if (data[k].type === "linear") {
          let $analysisItem = $(`
            <div id="${k}" class="data-item"
            style="padding:30px 30px;width:300px">
              <h3>${k}</h3>
              <p>baseline(${data[k].unit}): ${data[k].baseline.toFixed(2)}</p>
              <p>cycles: ${data[k].cycleNum}</p>
              <p>cycles per minute: ${data[k].cyclesPerMinute.toFixed(2)}</p>
              <p>cycle average time: ${data[k].cycleAvgTime.toFixed(2)}</p>
              <p>cycle average area: ${data[k].cycleAvgArea.toFixed(2)}</p>
              <p>max peak: ${data[k].peakMax.toFixed(2)}</p>
              <p>avg peak: ${data[k].peakAvg.toFixed(2)}</p>
              <p>num valleys: ${data[k].valleyNum}</p>
              <p>min valleys: ${data[k].valleyMin.toFixed(2)}</p>
              <p>avg valleys area: ${data[k].valleyAvg.toFixed(2)}</p>
              <p>variance: ${data[k].variance.toFixed(2)}</p>
            </div>
          `);
          $dataDropDown1.append(`<option value=${k}>${k}</option>`);
          $dataDropDown2.append(`<option value=${k}>${k}</option>`);
          $dataAnalysis.append($analysisItem);
        }
        else if (data[k].type === "static") {
          let $analysisItem = $(`
            <div id="${k}" class="data-item"
            style="padding:30px 30px;width:300px;">
              <h3>${k}</h3>
              <p>max: ${data[k].max.toFixed(2)} ${data[k].unit}</p>
              <p>min: ${data[k].min.toFixed(2)} ${data[k].unit}</p>
              <p>avg: ${data[k].avg.toFixed(2)} ${data[k].unit}</p>
            </div>
          `);
          $dataAnalysis.append($analysisItem);
        }
      });
    }

    populateDataModalFields(data);

  });
}

module.exports = {
  loadDataModal: receiveData,
};
