# EMMS application
This Program is meant to read off output from a serial connection, and graph out load cell values.
This program utilizes Electron, Documentation of which can be found here
https://electronjs.org/docs

[toc]

# Development Status
Main interface is complete. Majority of commit and time are bug fixes and UI enhancements

# TODO

## Installation (Windows)
1. Install the current NodeJS version for windows
2. Once that's complete, open `Command Prompt` in windows 7, or `Powershell` in windows 10 as administrator
3. Type in `node -v` and enter to verify that node is working
    - You should get a version number back
4. Type `npm install -g windows-build-tools` and then enter
    - This will download Visual Studio dev tools and python, both of which are required
      - This download takes a long time to complete
5. Once completed, navigate to this folder in `Command Prompt` or `Powershell`
6. Install dependencies using `npm install`
    - This will also rebuild `serialport` which will be platform dependent
7. Run `npm start` to start the application

## Installation (On everything else)
1. Install the current NodeJS version appropriate to your system
    - https://nodejs.org/en/download/current/
2. Change directory to this folder
    - `cd emms_application`
3. Install dependencies using `npm install`
4. Run the application using `npm start`

## Release
- Package application
  - For the host environment
    - `npm run package`
  - For everyone!
    - `npm run package-all`

## Troubleshooting
### Error on install: Electron rebuild
If you get an error during the install involving Electron rebuild, you may have to install electron alone.
1. From this directory (`emms20team/emms_application`) run `npm install electron`
2. Once it's finished, rerun `npm install`

### Error during run: Cannot connect to serial
Possible driver issue, but most likely some interference with the serial port.
Try unplugging the serial cable and plugging it back in, then rediscovering, and trying again. This will ususally fix these types of issues.